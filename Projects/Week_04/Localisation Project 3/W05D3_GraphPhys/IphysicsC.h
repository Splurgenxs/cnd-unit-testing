#pragma once 

#ifndef _IphysicsC_
#define _IphysicsC_

#include <glm/matrix.hpp>

class IphysicsC
{
public:
	IphysicsC()
	{};
	virtual~IphysicsC()
	{};

	virtual void Update(float dt) = 0;
	virtual void SetModelMatrix(glm::mat4& mat) {};

protected:

	
private:

};

#endif