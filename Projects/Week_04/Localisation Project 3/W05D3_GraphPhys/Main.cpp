#include <iostream>
#include "AACube.h"
#include "FSphere.h"
#include "PEmitter.h" 

Vec3d minE;
Vec3d maxE;

float radiusOfS;

Vec3d updateCenter;

using namespace std;

void main()
{
	//Setting the cube object
	AACube* CubeObject = new AACube();
	CubeObject->setCenter(Vec3d(5.0,0.0,0.0));
	CubeObject->setdepth(2);
	CubeObject->setheight(2); //since  a cube all paras are equidistant
	CubeObject->setwidth(2);

	CubeObject->update(minE, maxE);
	
	//setting the sphere Object
	FSphere* SphereObject = new FSphere();
	SphereObject->setCenter(Vec3d(2.0,0.0,0.0));
	SphereObject->setDiameter(4);
	
	//Raduis of sphere = 
	radiusOfS = (SphereObject->getDiameter()) / 2;

	//seting the particles
	ParticleEmitter* emitterObj = new ParticleEmitter();
	

	for (int i = 0;; i++)
	{
		updateCenter.x = SphereObject->getCenter().x + i;
		if (CubeObject->cubeSphereColl(minE, maxE, updateCenter, radiusOfS))
		{
			cout << "collison detected" << endl;
			break;
		}
		else
		{
			cout << "no collision"<<endl;
		}
	}


}