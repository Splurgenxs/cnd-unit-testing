#ifndef _CNameValuePair_HG_
#define _CNameValuePair_HG_

#include <string>
#include "CVector4f.h"

class CNameValuePair
{
public:
	CNameValuePair() : iValue(0), fValue(0.0f) {};
	// A few helpful constructors
	CNameValuePair( std::string init_name );
	CNameValuePair( int init_iValue );	// Note there's no name here
	CNameValuePair( float init_fValue );	// Note there's no name here
	CNameValuePair( CVector4f init_vec4Value );
	CNameValuePair( std::string init_name, std::string init_sValue );
	CNameValuePair( std::string init_name, int init_iValue );
	CNameValuePair( std::string init_name, float init_fValue );	
	CNameValuePair( std::string init_name, CVector4f init_vec4Value );

	~CNameValuePair() {};

	std::string name;
	std::string sValue;
	int iValue;
	float fValue;
	CVector4f vec4Value;
};

#endif