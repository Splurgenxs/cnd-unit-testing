#include "global.h"

#define WINDOW_TITLE_PREFIX "Chapter 4"
#include <iostream>		// Added
#include <sstream>		// Added

#include <vector>
#include <fstream>


//=========================CnD proj 3 Done=========================//

#include <glfw\glfw3.h>
#include <Windows.h>

//================================================================//

//---------------------------------------------//

CHRTimer g_simTimer;
std::vector< CGameObject* > vec_pGameObjects;


using namespace std;
void LoadShaders(void);
CPlyInfo tempPlyInfo;

//---------------------------------------------

void loadLanguange()
{
	//Load xml file
	std::wstring xmlLanguageFileName = L"localization.xml";

	pugi::xml_document doc;

	pugi::xml_parse_result result = doc.load_file(xmlLanguageFileName.c_str());

	if (!result)
	{
		OutputDebugStringW(L"Unable to open XML file.");
		exit(EXIT_FAILURE);
	}

	pugi::xml_node languages = doc.child("Localization");

	std::wostringstream ws;

	for (pugi::xml_node_iterator it = languages.begin(); it != languages.end(); it++)
	{

		//Loop attributes
		for (pugi::xml_attribute_iterator ait = it->attributes_begin(); ait != it->attributes_end(); ait++)
		{
			ws.str(L"");
			ws << ait->value();
			if (ws.str() == L"EN")
			{
				ws.str(L"");
				ws << it->child("String_1").child_value();
				_mapLanguage[LANGUAGE_ENGLISH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_2").child_value();
				_mapLanguage[LANGUAGE_ENGLISH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_3").child_value();
				_mapLanguage[LANGUAGE_ENGLISH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_4").child_value();
				_mapLanguage[LANGUAGE_ENGLISH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_5").child_value();
				_mapLanguage[LANGUAGE_ENGLISH].push_back(ws.str());
			}
			else if (ws.str() == L"FR")
			{
				ws.str(L"");
				ws << it->child("String_1").child_value();
				_mapLanguage[LANGUAGE_FRENCH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_2").child_value();
				_mapLanguage[LANGUAGE_FRENCH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_3").child_value();
				_mapLanguage[LANGUAGE_FRENCH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_4").child_value();
				_mapLanguage[LANGUAGE_FRENCH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_5").child_value();
				_mapLanguage[LANGUAGE_FRENCH].push_back(ws.str());
			}
			else if (ws.str() == L"SP")
			{
				ws.str(L"");
				ws << it->child("String_1").child_value();
				_mapLanguage[LANGUAGE_SPANISH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_2").child_value();
				_mapLanguage[LANGUAGE_SPANISH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_3").child_value();
				_mapLanguage[LANGUAGE_SPANISH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_4").child_value();
				_mapLanguage[LANGUAGE_SPANISH].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_5").child_value();
				_mapLanguage[LANGUAGE_SPANISH].push_back(ws.str());
			}
			else if (ws.str() == L"HI")
			{
				ws.str(L"");
				ws << it->child("String_1").child_value();
				_mapLanguage[LANGUAGE_HINDI].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_2").child_value();
				_mapLanguage[LANGUAGE_HINDI].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_3").child_value();
				_mapLanguage[LANGUAGE_HINDI].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_4").child_value();
				_mapLanguage[LANGUAGE_HINDI].push_back(ws.str());
				ws.str(L"");
				ws << it->child("String_5").child_value();
				_mapLanguage[LANGUAGE_HINDI].push_back(ws.str());
			}
		}
	}

}
//=====================================================================//
int main(int argc, char* argv[])
{

	::OpenGL_Initialize( argc, argv, 1200, 800 );

	LoadShaders();//Loads the Shaders
	SetShaderUniformVariables();
	
	g_pModelLoader = new CModelLoaderManager();		// CModelLoaderManager
	std::vector< std::string > vecModelsToLoad;
	
//=================//First entry point for Models=================//

	vecModelsToLoad.push_back( "ply/spherefrme.ply" );
	vecModelsToLoad.push_back( "ply/Seafloor2_onlyXYZ.ply" );
	vecModelsToLoad.push_back("ply/Utah_Teapot_onlyXYZ.ply");
	vecModelsToLoad.push_back("ply/bun_zipper_res3_onlyXYZ.ply");

	if (!g_pModelLoader->LoadModels(vecModelsToLoad))
	{
		std::cout << "Can't load one or more models. Sorry it didn't work out." << std::endl;
		return -1;
	}
//===================================================================//


//---------Setting Properties of Rendered OBject via Mediator--------------//
	
	g_pFactoryMediator = new CFactoryMediator();
	//unsigned int ID;

	ID = g_pFactoryMediator->CreateObjectByType( "Seafloor 2" );
	g_pFactoryMediator->UpdateObjectPositionByID( ID, CVector3f( 0.0f, -0.5f, 0.0f ) );
	g_pFactoryMediator->UpdateColourByID( ID, CVector3f( 62.0f/255.0f, 176.0f/255.0f, 56.0f/255.0f ) );
	g_pModelLoader->GetRenderingInfoByModelFileName( "ply/Seafloor2_onlyXYZ.ply", tempPlyInfo );
	g_pFactoryMediator->UpdateObjectScaleByID( ID, 25.0f / tempPlyInfo.extent );
	g_pFactoryMediator->UpdateObjectVelocityByID( ID, CVector3f( 0.0f, 0.0f, 0.0f ) );
	g_pFactoryMediator->UpdateWireframePropertyByID(ID, true);

	ID = g_pFactoryMediator->CreateObjectByType("Sphere");
	g_pFactoryMediator->UpdateObjectPositionByID(ID, CVector3f(0.0f, 1.0f, 0.0f));
	g_pFactoryMediator->UpdateColourByID(ID, CVector3f(0.0f,1.0f,0.0f));
	g_pModelLoader->GetRenderingInfoByModelFileName("ply/spherefrme.ply", tempPlyInfo);
	g_pFactoryMediator->UpdateObjectScaleByID(ID, 1);
	g_pFactoryMediator->UpdateObjectVelocityByID(ID, CVector3f(0.0f, 0.0f, 0.1f));
	g_pFactoryMediator->UpdateWireframePropertyByID(ID, false);

	ID = g_pFactoryMediator->CreateObjectByType("Teapot");
	g_pFactoryMediator->UpdateObjectPositionByID(ID, CVector3f(0.0f, 1.0f, 0.0f));
	g_pFactoryMediator->UpdateColourByID(ID, CVector3f(0.4f, 0.8f, 1.0f));
	g_pModelLoader->GetRenderingInfoByModelFileName("ply/Utah_Teapot_onlyXYZ.ply", tempPlyInfo);
	g_pFactoryMediator->UpdateObjectScaleByID(ID, 1.0f / tempPlyInfo.extent);
	g_pFactoryMediator->UpdateObjectVelocityByID(ID, CVector3f(0.1f, 0.0f, 0.0f));
	g_pFactoryMediator->UpdateWireframePropertyByID(ID, false);

	ID = g_pFactoryMediator->CreateObjectByType("Bunny");
	g_pFactoryMediator->UpdateObjectPositionByID(ID, CVector3f(0.0f, 1.0f, 0.0f));
	g_pFactoryMediator->UpdateColourByID(ID, CVector3f(1.0f, 1.0f, 0.0f));
	g_pModelLoader->GetRenderingInfoByModelFileName("ply/bun_zipper_res3_onlyXYZ.ply", tempPlyInfo);
	g_pFactoryMediator->UpdateObjectScaleByID(ID, 1.0f / tempPlyInfo.extent);
	g_pFactoryMediator->UpdateObjectVelocityByID(ID, CVector3f(0.0f, 0.1f, 0.0f));
	g_pFactoryMediator->UpdateWireframePropertyByID(ID, false);




//-----------------------------------------------------------------------//

	g_pFactoryMediator->getPhysicsObjects(vec_pGameObjects);

//--------------------------Manage Camera------------------------------//

//---------------------------------------------------------------------//
	g_pCamera = new CCamera();
	g_pCamera->SetMediator( (IMediator*)g_pFactoryMediator );
	g_pCamera->eye.x = 0.0f;		// Centred (left and right)
	g_pCamera->eye.y = 2.0f;		// 2.0 units "above" the "ground"
	g_pCamera->eye.z = 4.0f;		// 4.0 units "back" from the origin
	//g_pCamera->target    CVector3f gets set to 0,0,
	g_pCamera->up.x = 0.0f;
	g_pCamera->up.y = 1.0f;				// The Y axis is "up and down"
//---------------------------------------------------------------------//
	g_simTimer.Reset();
	g_simTimer.Start();		// Start "counting"

//-------------------------------Cnd Proj3 -Done------------------------------------//
	loadLanguange();//---->Load languag function.

	if (!_gl_font.Create("orange48Font.glf", 1))
	{
		return FALSE;
	}
//----------------------------------Cnd Proj3 -Done-----------------------------------//
	glutMainLoop();
//------------------------------------Cnd Proj3 -Done------------------------
	_gl_font.Destroy();
	exit(EXIT_SUCCESS);
//---------------------------------------Cnd Proj3 -Done---------------------------=--//
}

//used in idle function 
static const float g_MAXIMUM_TIME_STEP = 0.1f;		// 10th of a second or 100ms 

void IdleFunction(void)
{
	if (pauseState == false)
	{
	
		//===================SETTING THE TIMER============================//
		g_simTimer.Stop();
		float deltaTime = ::g_simTimer.GetElapsedSeconds();

		if (deltaTime > g_MAXIMUM_TIME_STEP)
		{
			deltaTime = g_MAXIMUM_TIME_STEP;
		}
		g_simTimer.Start();
		//================================================================//

		g_pCamera->eye.y = vec_pGameObjects[1]->position.y;//ss------->Camera look pos fixed

		g_pFactoryMediator->getPhysicsObjects(vec_pGameObjects);

		for (int index = 0; index != static_cast<int>(vec_pGameObjects.size()); index++)
		{
			vec_pGameObjects[index]->position.x += (vec_pGameObjects[index]->velocity.x * deltaTime);
			vec_pGameObjects[index]->position.y += (vec_pGameObjects[index]->velocity.y * deltaTime);
			vec_pGameObjects[index]->position.z += (vec_pGameObjects[index]->velocity.z * deltaTime);
		}
		for (int index = 0; index != static_cast<int>(vec_pGameObjects.size()); index++)
		{
			vec_pGameObjects[index]->Update(deltaTime);
		}

		::g_pCamera->Update(deltaTime);



		glutPostRedisplay();
		return;
	}
	
}

void TimerFunction(int Value)
{
  if (0 != Value)
  {
	std::stringstream ss;
	ss << WINDOW_TITLE_PREFIX << ": " 
		<< ::g_FrameCount * 4 << " Frames Per Second @ "
		<< ::g_screenWidth << " x"				// << CurrentWidth << " x "
		<< ::g_screenHeight;					// << CurrentHeight;

    glutSetWindowTitle(ss.str().c_str());

  }

  ::g_FrameCount = 0;	// FrameCount = 0;
  glutTimerFunc(250, TimerFunction, 1);

  return;
}

void LoadShaders(void)
{
	g_ShaderProgram_ID = glCreateProgram();	

	ExitOnGLError("ERROR: Could not create the shader program");

	//g_ShaderIds[1] = LoadShader("SimpleShader.fragment.glsl", GL_FRAGMENT_SHADER);
	g_VertexShader_ID = LoadShader("SimpleShader.fragment.glsl", GL_FRAGMENT_SHADER);

	//g_ShaderIds[2] = LoadShader("SimpleShader.vertex.glsl", GL_VERTEX_SHADER);
	g_FragmentShader_ID = LoadShader("SimpleShader.vertex.glsl", GL_VERTEX_SHADER);

	// Now we associate these specific COMPILED shaders to the "program"...
	//glAttachShader(g_ShaderIds[0], g_ShaderIds[1]);
	glAttachShader( g_ShaderProgram_ID, g_VertexShader_ID );
	//glAttachShader(g_ShaderIds[0], g_ShaderIds[2]);
	glAttachShader( g_ShaderProgram_ID, g_FragmentShader_ID );
	glLinkProgram( g_ShaderProgram_ID );

	ExitOnGLError("ERROR: Could not link the shader program");

  return;
}


