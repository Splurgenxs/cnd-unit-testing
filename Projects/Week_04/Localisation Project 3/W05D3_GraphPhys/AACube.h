#ifndef _Acube_
#define _Acube_


#include "CVector3f.h"

class AACube
{
public:
	AACube();
	~AACube();

public:

	void setCenter(CVector3f center);
	CVector3f getCenter();

	void setheight(float height);
	float getheight();
	
	void setwidth(float width);
	float getwidth();

	void setdepth(float depth);
	float getdepth();


	void update(CVector3f &minpoint, CVector3f &maxpoint);
	bool cubeSphereColl(CVector3f minpt, CVector3f maxpt, CVector3f S, float R);

	bool InBox;
	bool GetIntersection;
	bool CheckLineBox;
	
private:
	CVector3f center;
	float height;
	float width;
	float depth;

	float dist_squared;

};

#endif