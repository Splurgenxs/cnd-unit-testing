#include "CModelLoaderManager.h"
#include "global.h"				// Only for ExitOnGLError()




CModelLoaderManager::CModelLoaderManager()
{
	return;
}
CModelLoaderManager::~CModelLoaderManager()
{
	return;
}

bool CModelLoaderManager::LoadModels(std::vector<std::string> vecFileNames)
{
	// Go through the vector of file names, loading each one, 
	//	then storing the information into the private vector...
	for (int index = 0; index != vecFileNames.size(); index++)
	{
		std::string theFileName = vecFileNames[index];

		CPlyInfo tempModelInfo;

		if (!this->m_LoadPlyFileIntoGLBuffers(
			theFileName, tempModelInfo.vertexPositionBufferID,
			tempModelInfo.vertexColourBufferID,
			tempModelInfo.indexBufferID,
			tempModelInfo.numberOfVertices,
			tempModelInfo.numberOfElements,
			tempModelInfo.extent))
		{	// Something is very wrong... 
			return false;
		}

		tempModelInfo.plyFileASCII = theFileName;
		this->m_VecModels.push_back(tempModelInfo);

		 from_modelLoader_VertInfo = tempModelInfo.numberOfVertices;
	}


	return true;
}

bool CModelLoaderManager::GetRenderingInfoByModelFileName(std::string modelName,
	CPlyInfo &theInfo)
{
	// Search through the vector until we find the model we're looking for
	for (int index = 0; index != (int)this->m_VecModels.size(); index++)
	{	// Is this The One?
		if (this->m_VecModels[index].plyFileASCII == modelName)
		{	// Yup, it is
			theInfo = this->m_VecModels[index];
			return true;
		}
	}
	// Didn't find it.
	return true;
}


bool CModelLoaderManager::m_LoadPlyFileIntoGLBuffers( std::string plyFile,
							   unsigned int &vertexBufferID,					/* GLuint &vertexBufferID, */
							   unsigned int &vertexColourBufferID,				/* GLuint &vertexColourBufferID, */
							   unsigned int &indexBufferID,						/* GLuint &indexBufferID, */
							   int &numberOfVerticesLoaded, 
							   int &numberOfTrianglesLoaded, 
							   float &maxExtent )
{
	CPlyFile5nt* pPlyLoader = new CPlyFile5nt();
	std::wstring error;	// Returns error info in this string
	if ( ! pPlyLoader->OpenPLYFile( this->ASCII_to_Unicode( plyFile ), error ) )
	{
		return false;
	}

	maxExtent = pPlyLoader->getMaxExtent();

	numberOfVerticesLoaded = pPlyLoader->GetNumberOfVerticies();


//=======================================PLY INTO ARRAY LOADER========================================
//=====================================================================================================

	//Vertex* p_arrayBunnyVertices = new Vertex[ numberOfVerticesLoaded ];
	CVertex_fXYZ_fRGB* p_arrayBunnyVertices = new CVertex_fXYZ_fRGB[numberOfVerticesLoaded];
	// Copy the data from the bunny vector to this array
	for (int vertIndex = 0; vertIndex != numberOfVerticesLoaded; vertIndex++)
	{
		PLYVERTEX tempVertex = pPlyLoader->getVertex_at(vertIndex);
		p_arrayBunnyVertices[vertIndex].Position[0] = tempVertex.xyz.x;
		p_arrayBunnyVertices[vertIndex].Position[1] = tempVertex.xyz.y;
		p_arrayBunnyVertices[vertIndex].Position[2] = tempVertex.xyz.z;
		p_arrayBunnyVertices[vertIndex].Position[3] = 1.0f;

		p_arrayBunnyVertices[vertIndex].Color[0] = tempVertex.red;		// Red
		p_arrayBunnyVertices[vertIndex].Color[1] = tempVertex.green;	// Green
		p_arrayBunnyVertices[vertIndex].Color[2] = tempVertex.blue;		// Blue
		p_arrayBunnyVertices[vertIndex].Color[3] = 1.0f;				// Alpha

		// Normal information soon to be added
	}


////=======================================PLY INTO VECTOR LOADER=========================================
////======================================================================================================
//
//	for (int vertIndex = 0; vertIndex != numberOfVerticesLoaded; vertIndex++)
//	{
//		PLYVERTEX tempVertex = pPlyLoader->getVertex_at(vertIndex); 
//
//		CVertex_fXYZ_fRGB tempCvertObj;
//
//		tempCvertObj.Position[0] = tempVertex.xyz.x;
//		tempCvertObj.Position[1] = tempVertex.xyz.y;
//		tempCvertObj.Position[2] = tempVertex.xyz.z;
//		tempCvertObj.Position[3] = 1.0;
//
//		tempCvertObj.Color[0] = tempVertex.red;
//		tempCvertObj.Color[1] = tempVertex.green;
//		tempCvertObj.Color[2] = tempVertex.blue;
//		tempCvertObj.Color[3] = 1.0;
//
//		p_arrayBunnyVertices.push_back(tempCvertObj);
//	}
////-===========================INDEX LOader=================================
	numberOfTrianglesLoaded = pPlyLoader->GetNumberOfElements();
	int numberOfIndices = numberOfTrianglesLoaded * 3;

	// ---------------------------------------------------------------------
	GLuint* p_bunnyIndices = new GLuint[ numberOfIndices ];
	//GLuint p_bunnyIndices[3851 * 3];

	for ( int triIndex = 0; triIndex < numberOfTrianglesLoaded; triIndex++ )	// Step by three
	{
		//CTriangle tempTri = vecTriangles[triIndex];
		PLYELEMENT tempTri = pPlyLoader->getElement_at(triIndex);
	  
		int index1 = (triIndex * 3);
		int index2 = (triIndex * 3) + 1;
		int index3 = (triIndex * 3) + 2;


		p_bunnyIndices[index1] = (GLuint)tempTri.vertex_index_1;
		p_bunnyIndices[index2] = (GLuint)tempTri.vertex_index_2;
		p_bunnyIndices[index3] = (GLuint)tempTri.vertex_index_3;
	
	}
//=============================================================================================
//=======================Index Elements into Vector============================================

	//for (int triIndex = 0; triIndex < numberOfTrianglesLoaded; triIndex++)
	//{
	//	PLYELEMENT tempTri = pPlyLoader->getElement_at(triIndex);

	//	p_bunnyIndices.push_back(tempTri);
	//}

//========================================================================================
	// Now set up the OpenGL buffers and copy the information over
	// Get an ID (aka "name") for the two-part vertex buffer
	glGenVertexArrays(1, &vertexBufferID );
	glBindVertexArray( vertexBufferID );
	ExitOnGLError("ERROR: Could not generate the VAO");
  
	// "Bind" them, which means they are ready to be loaded with data
	glGenBuffers(1, &vertexColourBufferID );
	glBindBuffer(GL_ARRAY_BUFFER, vertexColourBufferID);	
	ExitOnGLError("ERROR: Could not bind the VAO");

	// Figure out how big (in "system unit" aka "bytes") the local 
	//	array of vertex information is (i.e. how big is each vertex X number of vertices)
	//int bytesInVertexArray = numberOfVerticesLoaded * sizeof(Vertex);
	int bytesInVertexArray = numberOfVerticesLoaded * sizeof(CVertex_fXYZ_fRGB);

	// This does the actual copy of the date from the "client" (CPU side)
	//	to the GPU ("video card") side. 
	glBufferData(GL_ARRAY_BUFFER, bytesInVertexArray, p_arrayBunnyVertices, GL_STATIC_DRAW);//-->PRE CLOTH DEMO 
	//glBufferData(GL_ARRAY_BUFFER, bytesInVertexArray,&p_arrayBunnyVertices[0], GL_STREAM_DRAW);//->FOR CLOTH DEMO CHANGE
	ExitOnGLError("ERROR: Could not bind the VBO to the VAO");


	// Now do the same for the index buffer
	glGenBuffers(1, &indexBufferID );			// Get an ID (aka "name")

	//glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, BufferIds[2]);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indexBufferID );		
	ExitOnGLError("ERROR: Could not set VAO attributes" );

	// Figure out how many "system units" (i.e. "bytes") the index buffer is
	// Or, how many bytes is GLuint (on this system) X number of indices
	int bytesInIndexArray = numberOfIndices * sizeof(GLuint);
	// Copy the data from the CPU RAM to the Video card RAM
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, bytesInIndexArray,p_bunnyIndices, GL_STATIC_DRAW);
	ExitOnGLError("ERROR: Could not bind the IBO to the VAO");



	glEnableVertexAttribArray(0);			// Position
	glEnableVertexAttribArray(1);			// Colour
	//glEnableVertexAttribArray(2);			// Normal
	//glEnableVertexAttribArray(3);			// Textures as defined the vertex shader 
	

	ExitOnGLError("ERROR: Could not enable vertex attributes");

	glVertexAttribPointer(0,		// index or "slot" in the shader
	                      4,		// Number of variables: vec4 would be 4 32-bit variables
						  GL_FLOAT,	// Type: vec4 is float
		                  GL_FALSE, // "normalize" the values (or not)
						  sizeof(CVertex_fXYZ_fRGB),	// Number of bytes per vertex (the "stride")
						  (GLvoid*)0);		// Offset from vertex (position is first, so offset = 0

	ExitOnGLError("ERROR: Could not set VAO attributes");

	// http://stackoverflow.com/questions/3718910/can-i-get-the-size-of-a-struct-field-w-o-creating-an-instance-of-the-struct
	int offsetToColourInBytes = sizeof(((CVertex_fXYZ_fRGB*)0)->Color);

	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(CVertex_fXYZ_fRGB), (GLvoid*)offsetToColourInBytes);// Offset in bytes to Colour

	ExitOnGLError("ERROR: Could not set VAO attributes");

	glBindVertexArray(0);		

	ExitOnGLError("ERROR: Could not set VAO attributes");


	return true;
}


bool CModelLoaderManager::ShutDown(void)
{
	for ( std::vector< CPlyInfo >::iterator itModel = this->m_VecModels.begin();
		  itModel != this->m_VecModels.end(); ++itModel )
	{
		CPlyInfo tempInfo = *itModel;	

		glDeleteBuffers( 1, &(tempInfo.indexBufferID) );
		glDeleteBuffers( 1, &(tempInfo.vertexColourBufferID) );
		glDeleteBuffers( 1, &(tempInfo.vertexPositionBufferID) );

		// Set to zero, just in case
		itModel->vertexPositionBufferID = 0;
		itModel->vertexColourBufferID = 0;
		itModel->indexBufferID =0;
	}

	// Clear the vector as all the buffers are invalid
	this->m_VecModels.clear();

	return true;
}

// NOTE: There are WAY better (i.e. safer!) ways to do this. See MultiByteToWideChar(), etc. in windows
// But this is "good enough" for now
std::wstring CModelLoaderManager::ASCII_to_Unicode( std::string ASCIIstring )
{
	std::wstringstream ssReturnUNICODE;
	for ( std::string::iterator itChar = ASCIIstring.begin(); itChar != ASCIIstring.end(); itChar++ )
	{
		wchar_t theChar = static_cast<wchar_t>( *itChar );
		ssReturnUNICODE << theChar;
	}
	return ssReturnUNICODE.str();
}

std::string CModelLoaderManager::Unicode_to_ASCII( std::wstring UnicodeString )
{
	std::stringstream ssReturnASCII;
	for ( std::wstring::iterator itChar = UnicodeString.begin(); itChar != UnicodeString.end(); itChar++ )
	{
		char theChar = static_cast<char>( *itChar );
		ssReturnASCII << theChar;
	}
	return ssReturnASCII.str();
}
