#include "global.h"


void glutKeyboard_callback(unsigned char key, int x, int y)
{
	switch( key )
	{
	case ' ':		// Space
		{
			CMessage theMessage;
			theMessage.vecNVPairs.push_back( CNameValuePair( "StopMoving" ) );
			::g_pFactoryMediator->SendMessageToObject( g_Player_ID, 0, theMessage );
		}
		break;
//-------------------------------------------------------------
	case '1':
	{
		_currentLanguage = LANGUAGE_ENGLISH;
		d_keyretrun = 0;

		g_pFactoryMediator->UpdateObjectPositionByID(ID, CVector3f(0.0f, 1.0f, 0.0f));
		g_pFactoryMediator->UpdateObjectPositionByID(ID, CVector3f(0.0f, 0.1f, 0.0f));
		g_pFactoryMediator->UpdateObjectVelocityByID(ID, CVector3f(0.0f, -0.1f, -0.1f));

	break; 
	}
	case '2':
	{
		_currentLanguage = LANGUAGE_FRENCH;
		d_keyretrun = 1;
		g_pFactoryMediator->UpdateObjectPositionByID(ID, CVector3f(0.0f, 0.0f, 0.0f));
		g_pFactoryMediator->UpdateObjectVelocityByID(ID, CVector3f(0.0f, 0.0f, -0.1f));
	break; 
	}
	case '3':
	{
		_currentLanguage = LANGUAGE_SPANISH;
		d_keyretrun = 2;
		g_pFactoryMediator->UpdateObjectPositionByID(ID, CVector3f(0.0f, 0.0f, 0.0f));
	break; 
	}
	case '4':
	{
		_currentLanguage = LANGUAGE_HINDI;
		d_keyretrun = 3;
		g_pFactoryMediator->UpdateObjectPositionByID(ID, CVector3f(0.0f, 0.0f, 0.0f));
	break; 
	}

	case 'O': case 'o':
	{
		KeyCalbackReturn = 1;
		break;
	}

		
	case 'P':case 'p':
	{
		
		pauseState =! pauseState;
		break;
	}
//-------------------------------------------------------------

	case 'w': case 'W':
		::g_pCamera->eye.x -= 0.5f;		// Move the camera "left" (-ve x)
		//::g_ourObjects[2].y += 0.1f;	// Up
		//g_ourObjects[2].velY += 0.01f;
		//g_ourObjects[2].m_State = "GOING_UP";
		break;
	case 's': case 'S':
		::g_pCamera->eye.x += 0.5f;		// Right (+ve x)
		//::g_ourObjects[2].y -= 0.1f;	// Down
		//::g_ourObjects[2].velY -= 0.01f;	// Down
		//g_ourObjects[2].m_State = "GOING_DOWN";
		break;
	case 'a': case 'A':
		::g_pCamera->eye.z -= 0.5f;		// Forward (-ve z)
		//::g_ourObjects[2].x -= 0.1f;	// Left
		//::g_ourObjects[2].velX -= 0.01f;	// Left
		//g_ourObjects[2].m_State = "GOING_LEFT";
		break;
	case 'd': case 'D':
		::g_pCamera->eye.z += 0.5f;		// Back (-ve z)
		//::g_ourObjects[2].x += 0.1f;	// Right
		//::g_ourObjects[2].velX += 0.01f;	// Right
		//g_ourObjects[2].m_State = "GOING_RIGHT";
		break;

	/*case 'P': case 'p':     */        //no of P , strt vel , age, init pos ,time	
		/*::emitterObj->GenerateParticles(250, CVector3f(-1.0f, -2.0f, 1.1f), 0.0f, 10.0f, true);*/
		
		break;
	};

	return;
}

void glutSpecialKey_callback( int key, int x, int y )
{
	switch ( key )
	{
	case GLUT_KEY_UP:
		//::g_ourObjects[2].z += 0.1f;		// Away (along z axis)
		// Sending a bunny a command
		{
			CMessage theMessage;
			theMessage.vecNVPairs.push_back( CNameValuePair( "ChangeDirection", "Z" ) );
			theMessage.vecNVPairs.push_back( CNameValuePair( "NewZVel", 0.25f ) );
			::g_pFactoryMediator->SendMessageToObject( g_Player_ID, 0, theMessage );
		}
		break;
	case GLUT_KEY_DOWN:
		//::g_ourObjects[2].z -= 0.1f;		// Towards (along z axis)
		{
			CMessage theMessage;
			theMessage.vecNVPairs.push_back( CNameValuePair( "ChangeDirection", "Z" ) );
			theMessage.vecNVPairs.push_back( CNameValuePair( "NewZVel", -0.25f ) );
			::g_pFactoryMediator->SendMessageToObject( g_Player_ID, 0, theMessage );
		}
		break;
	case GLUT_KEY_LEFT:
		{
			CMessage theMessage;
			theMessage.vecNVPairs.push_back( CNameValuePair( "ChangeDirection", "X" ) );
			theMessage.vecNVPairs.push_back( CNameValuePair( "NewXVel", -0.25f ) );
			::g_pFactoryMediator->SendMessageToObject( g_Player_ID, 0, theMessage );
		}
		break;
	case GLUT_KEY_RIGHT:
		{
			CMessage theMessage;
			theMessage.vecNVPairs.push_back( CNameValuePair( "ChangeDirection", "X" ) );
			theMessage.vecNVPairs.push_back( CNameValuePair( "NewXVel", 0.25f ) );
			::g_pFactoryMediator->SendMessageToObject( g_Player_ID, 0, theMessage );
		}
		break;
	};
	return;
}