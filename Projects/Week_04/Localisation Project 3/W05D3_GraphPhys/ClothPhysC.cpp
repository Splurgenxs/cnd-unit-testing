//#include "ClothPhysC.h"
//
//
//
//ClothPhysC::ClothPhysC()
//{
//	    mTime = 0.0f;
//		mK = 100.6f;
//		mDamping = 0.99f;
//		mGravity.x = 0.0f; mGravity.y = - 9.81f; mGravity.z =  0.0f;
//		mMass = 1.0f;
//		mWind.x = 3.0f; mWind.y = 0.0f; mWind.z = 19.0f;
//
//
//	
//}
//ClothPhysC::~ClothPhysC()
//{
//
//}
//
//ClothPhysC::sSpring::sSpring()
//{
//
//}
//ClothPhysC::sSpring::sSpring(sNode* nodeA, sNode* nodeB, float length)
//{
//	 NodeA = nodeA;
//	 NodeB = nodeB;
//	 RestLength = length;
//	 Length = length;
//}
//
//ClothPhysC::sNode::sNode(unsigned int idxVert, float x, float y, float z)
//{
//	IdxVert=idxVert;
//	FixedPosition = false;
//	Position.x = x; Position.y = y; Position.z = z;
//	PreviousPosition.x = x; PreviousPosition.y = y; PreviousPosition.y = z;
//	Velocity.x = 0.0f, Velocity.x = 0.0f, Velocity.x = 0.0f;
//	Acceleration.x = 0.0f, Acceleration.y = 0.0f, Acceleration.z = 0.0f;
//}
//
//
//void ClothPhysC::Update(float dt)
//{
//	dt = glm::min(dt, 0.03f);
//	mTime += dt;
//	float halfDt = dt / 2.f;
//	float dtSquared = dt * dt;
//
//
//	unsigned int numNodes = mNodes.size();
//
//	for (unsigned int idxNode = 0; idxNode < numNodes; idxNode++)
//	{
//		if (mNodes[idxNode]->FixedPosition) continue;
//		sNode* node = mNodes[idxNode];
//		node->PreviousPosition = node->Position;
//
//		// step 1: r(t+h) = r(t) + h * [v(t) + (h/2)*a(t)]
//		node->Position += (node->Velocity + node->Acceleration * halfDt) * dt;
//
//		// step 2: v(t + h/2) = v(t) + (h/2)*a(t)
//		node->Velocity += node->Acceleration * halfDt;
//
//	}
//	// calculate a(t + h)
//	CalculateSpringForces();//===========================
//
//	// step 3: v(t + h) = v(t + h/2) + (h/2)*a(t + h)
//	for (unsigned int idxNode = 0; idxNode < numNodes; idxNode++)
//	{
//		if (mNodes[idxNode]->FixedPosition) continue;
//		sNode* node = mNodes[idxNode];
//		node->Velocity += node->Acceleration * halfDt;
//	}
//}
//
//void ClothPhysC::SetNodeFixed(unsigned int idx, bool value)
//{
//	mNodes[idx]->FixedPosition = value;
//}
//
//void ClothPhysC::CalculateSpringForces()
//{
//	// clear the existing spring forces
//	std::vector<sNode*>::iterator itNode = mNodes.begin();
//	while (itNode != mNodes.end())
//	{
//		(*itNode)->SpringForce = glm::vec3(1.f, 1.f, 1.f);
//		itNode++;
//	}
//	// sum up the spring forces in the nodes
//	std::vector<sSpring*>::iterator itSpring = mSprings.begin();
//	while (itSpring != mSprings.end())
//	{
//		glm::vec3 vec = (*itSpring)->NodeB->Position - (*itSpring)->NodeA->Position;
//		float len = glm::length(vec);
//		vec /= len;
//
//		vec *= mK * (len - (*itSpring)->RestLength);
//
//		(*itSpring)->NodeA->SpringForce += vec;
//		(*itSpring)->NodeB->SpringForce -= vec;
//
//		itSpring++;
//	}
//
//	float windFactor = 1.0f + 0.4f * glm::sin(mTime);
//	glm::vec3 accWind = mWind * windFactor;
//
//
//	// calculate the damping and wind factors, then assign the Acceleration
//	unsigned int numVerts = mNodes.size();
//	for (unsigned int idxVert = 0; idxVert < numVerts; idxVert++)
//	{
//		glm::vec3 accSpring = mNodes[idxVert]->SpringForce / mMass;
//		glm::vec3 accDamping = mNodes[idxVert]->Velocity * mDamping;
//		mNodes[idxVert]->Acceleration = mGravity + accWind + accSpring - accDamping;
//	}
//}
//
//void ClothPhysC::SetSpringLengths()
//{
//	std::vector<sSpring*>::iterator itSpring = mSprings.begin();
//	while (itSpring != mSprings.end())
//	{
//		(*itSpring)->Length = glm::length((*itSpring)->NodeA->Position - (*itSpring)->NodeB->Position);
//		itSpring++;
//	}
//}
//
//void ClothPhysC::AddSpring(unsigned int idxA, unsigned int idxB)
//{
//	sNode* nodeA = mNodes[idxA];
//	sNode* nodeB = mNodes[idxB];
//
//	std::map<unsigned int, sSpring*>::iterator itSpring;
//
//	sSpring* springFromA = new sSpring();
//	sSpring* springFromB = new sSpring();
//
//	bool springA = false;
//	bool springB = false;
//
//	itSpring = nodeA->Springs.find(idxB);
//	if (itSpring != nodeA->Springs.end())
//	{
//		springFromA = itSpring->second;
//		springA = true;
//	}
//
//	itSpring = nodeB->Springs.find(idxA);
//	if (itSpring != nodeB->Springs.end())
//	{
//		springFromB = itSpring->second;
//		springB = true;
//	}
//
//	if (springA)
//	{
//		if (springB)
//		{
//			return;
//		}
//		nodeB->Springs[idxA] = springFromA;
//	}
//	else if (springB)
//	{
//		nodeA->Springs[idxB] = springFromB;
//	}
//	else
//	{
//		float len = glm::length(mNodes[idxA]->Position - mNodes[idxB]->Position);
//		sSpring* spring(new sSpring(nodeA, nodeB, len));
//		nodeA->Springs[idxB] = spring;
//		nodeB->Springs[idxA] = spring;
//		mSprings.push_back(spring);
//	}
//}
//
//void ClothPhysC::SetSpringsFromTriangulatedIndices(const std::vector<unsigned int>& indices)
//{
//	unsigned int numIndices = indices.size();
//	for (unsigned int idxIndex = 0; idxIndex < numIndices; idxIndex += 3)
//	{
//		unsigned int idxA = indices[idxIndex];
//		unsigned int idxB = indices[idxIndex + 1];
//		unsigned int idxC = indices[idxIndex + 2];
//		AddSpring(idxA, idxB);
//		AddSpring(idxB, idxC);
//		AddSpring(idxC, idxA);
//	}
//}
//
//void ClothPhysC::SetVertices(const std::vector<CVertex_fXYZ_fRGB>& vertices, float radius)
//{
//
//	mNodes.clear();
//	if (vertices.empty())
//	{
//		throw std::exception("woah woah woah... they've been set");
//	}
//	mRadius = radius;
//	mNodes.resize(vertices.size());
//	unsigned int numNodes = vertices.size();
//	for (unsigned int idx = 0; idx < numNodes; idx++)
//	{
//		const CVertex_fXYZ_fRGB* v = &vertices[idx];
//
//		sNode* idxMnode = new sNode(idx, v->Position[0], v->Position[1], v->Position[2]);
//			mNodes[idx] = idxMnode;
//	}  
//
//}
//
//void ClothPhysC::GetPosition(unsigned int idx, float* floatXYZ)
//{
//	sNode* node = mNodes[idx];
//	floatXYZ[0] = node->Position.x;
//	floatXYZ[1] = node->Position.y;
//	floatXYZ[2] = node->Position.z;
//}
//
//void ClothPhysC::setSprings(unsigned int idxA, unsigned int idxB, unsigned int idxC, unsigned int idxD)
//{
//	//setting springs
//	AddSpring(idxA, idxB);
//	AddSpring(idxA, idxC);
//	AddSpring(idxA, idxD);
//	AddSpring(idxC, idxB);
//	AddSpring(idxC, idxD);
//	AddSpring(idxB, idxD);
//
//}
//void ClothPhysC::ClothDemo(std::vector<CVertex_fXYZ_fRGB>& vertices)
//{
//
//	float width = 2.f;
//	int numNodesHoriz = 7;
//
//	SetVertices(vertices, (width / numNodesHoriz));
//
//	//setting nodes 
//
//	/*SetNodeFixed(0, true);*/
//	SetNodeFixed(1, true);
//	SetNodeFixed(2, true);
//	SetNodeFixed(3, true);
//	/*SetNodeFixed(4, true);*/
//
//}
//
////==========================CLOTH STREAM BUFFER MANUPULATION======================
////Meant to insert and retrieve data from the stream Buffer
//
//CVertex_fXYZ_fRGB* ClothPhysC::BeginMapping()
//{
//
//
//	GLsizei vertSize = sizeof(CVertex_fXYZ_fRGB);
//	glBindBuffer(GL_ARRAY_BUFFER, 1/*vercolIDVal*/);
//	numVerts = 45;
//	GLuint streamDataSize = numVerts * vertSize;
//
//	return (CVertex_fXYZ_fRGB*)glMapBufferRange(GL_ARRAY_BUFFER, 0, streamDataSize, GL_MAP_WRITE_BIT | GL_MAP_UNSYNCHRONIZED_BIT);
//}
//
//void ClothPhysC::EndMapping()
//{
//	glUnmapBuffer(GL_ARRAY_BUFFER);
//	glBindBuffer(GL_ARRAY_BUFFER, 0);
//}
//
//void ClothPhysC::UpdateClothGraphics()
//{
//
//	CVertex_fXYZ_fRGB* verts = BeginMapping();
//
//
//	for (unsigned int idxVert = 0; idxVert < numVerts; idxVert++)
//	{
//		GetPosition(idxVert, verts[idxVert].Position);
//	}
//	EndMapping();
//}
//
////==============================================================================