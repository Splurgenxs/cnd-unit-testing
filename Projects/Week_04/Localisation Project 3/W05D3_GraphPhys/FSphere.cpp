#include "FSphere.h"


FSphere::FSphere()
{

}
FSphere::~FSphere()
{

}

void FSphere::setCenter(CVector3f center)
{
	this->center = center;
}

CVector3f FSphere::getCenter()
{
	return center;
}

void FSphere::setDiameter(float diameter)
{
	this->diameter = diameter;
}

float FSphere::getDiameter()
{
	return diameter;
}


//For CCD.....................

void FSphere::setspUpdate_S_Pos(CVector3f newpos)
{
	this->spUpdate_S_Pos = getCenter();
}
CVector3f FSphere::getspUpdate_S_Pos()
{
	return spUpdate_S_Pos;
}

void FSphere::setspUpdate_L_Pos(CVector3f newpos)
{
	this->spUpdate_L_Pos = newpos;
}
CVector3f FSphere::getspUpdate_L_Pos()
{
	return spUpdate_L_Pos;
}


