#ifndef _VertexTypes_HG_
#define _VertexTypes_HG_

struct CVertex_fXYZ_fRGB
{
	CVertex_fXYZ_fRGB();	// Yes, structs can have constructors. It's true.
	float Position[4];
	float Color[4];

	//float Normal[4];

	void SetPosition(float x, float y, float z, float w = 1.0f);
	void SetColor(float r, float g, float b, float a = 1.0f);
	void SetNormal(float x, float y, float z, float w = 1.0f);

};

struct CVertex_fXYZ_fRGB_fNxNyNz
{
	CVertex_fXYZ_fRGB_fNxNyNz();	
	float Position[4];
	float Color[4];
	float Normal[4];

	void SetPosition(float x, float y, float z, float w = 1.0f);
	void SetColor(float r, float g, float b, float a = 1.0f);
	void SetNormal(float x, float y, float z, float w = 1.0f);
};

#endif
