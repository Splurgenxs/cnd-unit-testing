#ifndef _FSphere_
#define _FSphere_

#include "CVector3f.h"

class FSphere
{
public:
	FSphere();
	~FSphere();

public:
	void setCenter(CVector3f center);
	CVector3f getCenter();

	void setDiameter(float diameter);
	float getDiameter();

	void setspUpdate_S_Pos(CVector3f newpos);
	CVector3f getspUpdate_S_Pos();
	
	void setspUpdate_L_Pos(CVector3f newpos);
	CVector3f getspUpdate_L_Pos();


	CVector3f velocity;		// c'tor sets these to zeros
	float age;

	CVector3f line_intersecting_Plane();
	

private:
	CVector3f center;
	float diameter;

	CVector3f spUpdate_S_Pos;
	CVector3f spUpdate_L_Pos;
};

#endif