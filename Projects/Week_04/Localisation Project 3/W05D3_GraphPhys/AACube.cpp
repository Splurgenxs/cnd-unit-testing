#include "AACube.h"


AACube::AACube()
{
}
AACube::~AACube()
{
}

void AACube::setCenter(CVector3f center)
{
	this->center = center;
}
CVector3f AACube::getCenter()
{
	return center;
}

void AACube::setdepth(float depth){
	this->depth = depth;
}
float AACube::getdepth(){
	return depth;
}

void AACube::setheight(float height){
	this->height = height;
}
float AACube::getheight(){
	
	return depth;
}

void AACube::setwidth(float width)
{
	this->width = width;
}

float AACube::getwidth()
{
	return width;
}


void AACube::update(CVector3f &minpoint , CVector3f &maxpoint)
{
  //from what i could figure out to caluclate max and min point(i.e any two diognally aligned vertices in a cube) one needs to add  and subtract half the values of 
	//height , width and depth wrt x,y,z  respectively to get the Max and min points of the cube.
	//so :-

	minpoint.x = center.x - width/2;
	minpoint.y = center.y - height/2;
	minpoint.z = center.z - depth/2;

	maxpoint.x = center.x + width/2;
	maxpoint.y = center.y + height/2;
	maxpoint.z = center.z + depth/2;
}

bool AACube::cubeSphereColl(CVector3f minptC, CVector3f maxptC, CVector3f CenterS, float R)
{
		 dist_squared = R * R;
		
		if (CenterS.x < minptC.x)
		{ 
			dist_squared -= (CenterS.x - minptC.x)*(CenterS.x - minptC.x);
		}
		else if (CenterS.x > maxptC.x)
		{
			 dist_squared -= (CenterS.x - maxptC.x)*(CenterS.x - maxptC.x);
		}
		if (CenterS.y < minptC.y)
		{
			dist_squared -= (CenterS.y - minptC.y)*(CenterS.y - minptC.y);
		}
		else if (CenterS.y > maxptC.y)
		{
			dist_squared -= (CenterS.y - maxptC.y)*(CenterS.y - maxptC.y);
		}
		if (CenterS.z < minptC.z)
		{
			dist_squared -= (CenterS.z - minptC.z)*(CenterS.z - minptC.z);
		}
		else if (CenterS.z > maxptC.z)
		{
			dist_squared -= (CenterS.z - maxptC.z)*(CenterS.z - maxptC.z);
		}
		
		return dist_squared > 0;

	//http://stackoverflow.com/questions/4578967/cube-sphere-intersection-test
		//refrence
}

//=============================================CCD Test============================================

//bool ClipLine(int d, const AACube& aabbBox, const CVector3f& v0, const CVector3f& v1, float& f_low, float& f_high)
//{
//	// f_low and f_high are the results from all clipping so far. We'll write our results back out to those parameters.
//
//	// f_dim_low and f_dim_high are the results we're calculating for this current dimension.
//	float f_dim_low, f_dim_high;
//
//	// Find the point of intersection in this dimension only as a fraction of the total vector http://youtu.be/USjbg5QXk3g?t=3m12s
//	f_dim_low = (aabbBox.vecMin.v[d] - v0.v[d]) / (v1.v[d] - v0.v[d]);
//	f_dim_high = (aabbBox.vecMax.v[d] - v0.v[d]) / (v1.v[d] - v0.v[d]);
//
//	// Make sure low is less than high
//	if (f_dim_high < f_dim_low)
//		swap(f_dim_high, f_dim_low);
//
//	// If this dimension's high is less than the low we got then we definitely missed. http://youtu.be/USjbg5QXk3g?t=7m16s
//	if (f_dim_high < f_low)
//		return false;
//
//	// Likewise if the low is less than the high.
//	if (f_dim_low > f_high)
//		return false;
//
//	// Add the clip from this dimension to the previous results http://youtu.be/USjbg5QXk3g?t=5m32s
//	f_low = max(f_dim_low, f_low);
//	f_high = min(f_dim_high, f_high);
//
//	if (f_low > f_high)
//		return false;
//
//	return true;
//}
//
//// Find the intersection of a line from v0 to v1 and an axis-aligned bounding box http://www.youtube.com/watch?v=USjbg5QXk3g
//bool LineAABBIntersection(const AACube& aabbBox, const CVector3f& v0, const CVector3f& v1, CVector3f& vecIntersection, float& flFraction)
//{
//	float f_low = 0;
//	float f_high = 1;
//
//	if (!ClipLine(0, aabbBox, v0, v1, f_low, f_high))
//		return false;
//
//	if (!ClipLine(1, aabbBox, v0, v1, f_low, f_high))
//		return false;
//
//	if (!ClipLine(2, aabbBox, v0, v1, f_low, f_high))
//		return false;
//
//	// The formula for I: http://youtu.be/USjbg5QXk3g?t=6m24s
//	Vector b = v1 - v0;
//	vecIntersection = v0 + b * f_low;
//
//	flFraction = f_low;
//
//	return true;
//}

bool inline GetIntersection(float fDst1, float fDst2, CVector3f P1, CVector3f P2, CVector3f &Hit) 
{
	if ((fDst1 * fDst2) >= 0.0f)
	{
		return false;
	}
	if (fDst1 == fDst2)
	{
		return false;
	}
	Hit.x = P1.x + (P2.x - P1.x) * (-fDst1 / (fDst2 - fDst1));
	Hit.y = P1.y + (P2.y - P1.y) * (-fDst1 / (fDst2 - fDst1));
	Hit.z = P1.z + (P2.z - P1.z) * (-fDst1 / (fDst2 - fDst1));
	
	return true;
}

int inline InBox(CVector3f Hit, CVector3f minpoint, CVector3f maxpoint, const int Axis)
{
	if (Axis == 1 && Hit.z > minpoint.z && Hit.z < maxpoint.z && Hit.y > maxpoint.y && Hit.y < maxpoint.y)
	{
		return true;
	}
	if (Axis == 2 && Hit.z > minpoint.z && Hit.z < maxpoint.z && Hit.x > maxpoint.x && Hit.x < maxpoint.x)
	{
		return true;
	}
	if (Axis == 3 && Hit.x > minpoint.x && Hit.x < maxpoint.x && Hit.y > maxpoint.y && Hit.y < maxpoint.y)
	{
		return true;
	}
	return false;
}

// returns true if line (L1, L2) intersects with the box (B1, B2)
// returns intersection point in Hit

bool CheckLineBox(CVector3f B1, CVector3f B2, CVector3f L1, CVector3f L2, CVector3f &Hit)
{
	if (L2.x < B1.x && L1.x < B1.x)
	{
		return false;
	}
	if (L2.x > B2.x && L1.x > B2.x) 
	{
		return false;
	}
	if (L2.y < B1.y && L1.y < B1.y)
	{
		return false;
	}
	if (L2.y > B2.y && L1.y > B2.y)
	{
		return false;
	}
	if (L2.z < B1.z && L1.z < B1.z) 
	{
		return false;
	}
	if (L2.z > B2.z && L1.z > B2.z)
	{
		return false;
	}
	if (L1.x > B1.x && L1.x < B2.x && L1.y > B1.y && L1.y < B2.y && L1.z > B1.z && L1.z < B2.z)
	{
		Hit = L1;
		return true;
	}

	if ((GetIntersection(L1.x - B1.x, L2.x - B1.x, L1, L2, Hit) && InBox(Hit, B1, B2, 1))
		|| (GetIntersection(L1.y - B1.y, L2.y - B1.y, L1, L2, Hit) && InBox(Hit, B1, B2, 2))
		|| (GetIntersection(L1.z - B1.z, L2.z - B1.z, L1, L2, Hit) && InBox(Hit, B1, B2, 3))
		|| (GetIntersection(L1.x - B2.x, L2.x - B2.x, L1, L2, Hit) && InBox(Hit, B1, B2, 1))
		|| (GetIntersection(L1.y - B2.y, L2.y - B2.y, L1, L2, Hit) && InBox(Hit, B1, B2, 2))
		|| (GetIntersection(L1.z - B2.z, L2.z - B2.z, L1, L2, Hit) && InBox(Hit, B1, B2, 3)))
	{
		return true;
	}
	return false;
}
//==============================================================================================