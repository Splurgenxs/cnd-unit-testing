
#ifndef _global_HG_
#define _global_HG_

//#include "Utils.h"		// Contains Vertex struct, but no longer neededd
#include <time.h>			// Only needed for the "clock_t g_LastTime" variable

#define GLM_FORCE_CXX98    
#define GLM_FORCE_RADIANS
#include <glm/glm.hpp>
#include <glm/mat4x4.hpp> // glm::mat4


#include "CGameObject.h"				
#include "CTriangle.h"		
#include "CFactoryMediator.h"	
#include "CModelLoaderManager.h"

#include "CHRTimer.h"
#include "CPlyInfo.h"

// Added October 3, 2014
#include "CCamera.h"

//---------------------------Localisation CND DOne-----------------------------------

#include <glfont2.h>
#include <pugixml.hpp>
enum eLanguage
{
	LANGUAGE_ENGLISH = 0,
	LANGUAGE_FRENCH,
	LANGUAGE_SPANISH,
	LANGUAGE_HINDI,
	LANGUAGE_NONE
};

enum eText
{
	STRING_1 = 0,
	STRING_2,
	STRING_3,
	STRING_4,
	STRING_5
};


extern glfont::GLFont _gl_font;
extern std::map<eLanguage, std::vector<std::wstring>> _mapLanguage;
extern eLanguage _currentLanguage;
extern int KeyCalbackReturn;

extern bool pauseState;

extern  int d_keyretrun ;

extern unsigned int ID;

//-------------------------------------------------------------

using namespace std;

extern CCamera* g_pCamera;

extern CModelLoaderManager* g_pModelLoader;	// "p" for pointer
extern CFactoryMediator* g_pFactoryMediator;			
extern unsigned int g_Player_ID;	// = 0;		// Used to locate the player


//Particle obj==========================================
//extern ParticleEmitter* emitterObj;
//extern vector< CGameObject* > PhysDebugObjects;
//======================================================


extern glm::mat4 g_matProjection;
extern glm::mat4 g_matView;
extern glm::mat4 g_matWorld;		// aka "Model" matrix

extern GLuint g_ProjectionMatrixUniformLocation;			
extern GLuint g_ViewMatrixUniformLocation;				
extern GLuint g_ModelMatrixUniformLocation;				
extern GLuint g_ObjectColourUniformLocation;				

extern GLuint g_slot_LightPosition;	//uniform vec4 LightPosition;	
extern GLuint g_slot_LightColour;	//uniform vec4 LightColour;		
extern GLuint g_slot_attenuation;	//uniform float attenuation;	

extern GLuint g_ShaderProgram_ID;
extern GLuint g_VertexShader_ID;
extern GLuint g_FragmentShader_ID;

extern int g_screenWidth;	// = 1400;
extern int g_screenHeight;	// = 900;
extern int g_windowHandle;	// = 0;
extern unsigned int g_FrameCount;	// = 0;
extern clock_t g_LastTime;		// = 0;		

// Since this is static (i.e. there's only one of these), so we don't need "extern"
static const std::string g_WindowTitleDefault = "OpenGL for the win!";

bool SetShaderUniformVariables(void);

bool LoadPlyFileIntoGLBuffers( std::string plyFile,
							   GLuint &vertexBufferID, 
							   GLuint &vertexColourBufferID, 
							   GLuint &indexBufferID, 
							   int &numberOfVerticesLoaded, 
							   int &numberOfTrianglesLoaded, 
							   float &maxExtent );

bool OpenGL_Initialize(int argc, char* argv[], int screenWidth, int screenHeight);

// Loads the "whats_this.txt" file and displays it in the console
void printTheWhatsThisProgramAboutBlurb(void);

// OpenGL callbacks
void glutReshape_callback(int Width, int Height);		// void ResizeFunction(int, int);
void glutDisplay_callback(void);						// void RenderFunction(void);
void TimerFunction(int);
void IdleFunction(void);
void glutClose_callback(void);							// void DestroyCube(void);

// ADDED in class on September 19, 2014
void glutKeyboard_callback(unsigned char key, int x, int y);	// void keyboardFunctionCallback(unsigned char key, int x, int y);
void glutSpecialKey_callback( int key, int x, int y );			// void specialKeyCallback( int key, int x, int y );


void ExitOnGLError(const char* error_message);						// from Util.cpp
GLuint LoadShader(const char* filename, GLenum shader_type);		// from Util.cpp

static const double PI = 3.14159265358979323846;		// Was in Util.h, but super helpful, yo!






				
#endif
