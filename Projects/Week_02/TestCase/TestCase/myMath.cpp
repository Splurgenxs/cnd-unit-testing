#include "myMath.h"

myMath::myMath(){}

//returns n! (the factorial of n). For negative n, n! is defined to be 1
int myMath::Factorial(int n)
{
	int result = 1;
	for (int i = 1; i <= n; i++)
	{
		result *= i;
	}

	return result;
}

bool myMath::IsPrime(int n)
{
	//trivial case 1: small numbers
	if (n < 1) return false;
	//trivial case 2: even numbers
	if (n % 2 == 0) return n == 2;

	//now, we have n is odd and n >= 3
	for (int i = 3;; i += 2)
	{
		//we only have to try i up to the square root of n
		if (i > n / i) break;

		//now we have i <= n/i < n.
		//if n is divisible by i, n is not prime. 
		if (n % i == 0) return false;
	}

	return true;
}

myMath::~myMath(){}
