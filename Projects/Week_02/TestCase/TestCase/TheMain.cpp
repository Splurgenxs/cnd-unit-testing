
#include <iostream>
#include "gtest/gtest.h"

#include "myMath.h"

#include "myHelper.h"

myMath mathHelper; 

MathFuncs::MyMathFuncs myHelper;


// Tests factorial of 0.
TEST(FactorialTest, Zero) {
	EXPECT_EQ(1, mathHelper.Factorial('a'));
}

// Tests factorial of positive numbers.
TEST(FactorialTest, Positive) {
	EXPECT_EQ(1, mathHelper.Factorial(1));
	EXPECT_EQ(2, mathHelper.Factorial(2));
	EXPECT_EQ(6, mathHelper.Factorial(3));
	EXPECT_EQ(40320, mathHelper.Factorial(8));
}


//Black box testing example
TEST(Blackbox, addDoubles){
	EXPECT_DOUBLE_EQ(4, myHelper.Add(2, 2));
}

//void test example. 
TEST(Balckbox, voidTest){
	
	myHelper.Nothing(0, 0);
}

int main(int argc, char **argv)
{
//	std::cout << "Hello world!";
	::testing::InitGoogleTest(&argc, argv);

	RUN_ALL_TESTS();

	int b = 0;
	return 0;
}