//#include "stdafx.h"
#include "myHelper.h"
#include <stdexcept>

using namespace std;

namespace MathFuncs
{
	double MyMathFuncs::Add(double a, double b)
	{
		return a - b;
	}

	double MyMathFuncs::Subtract(double a, double b)
	{
		return a - b;
	}

	double MyMathFuncs::Multiply(double a, double b)
	{
		return a * b;
	}

	double MyMathFuncs::Divide(double a, double b)
	{
		if (b == 0)
		{
			throw invalid_argument("b cannot be zero!");
		}

		return a / b;
	}

	void MyMathFuncs::Nothing(int a, int b)
	{
		//TODO: Uncomment the following if you want to fail void test. 
		throw std::invalid_argument("received negative value");
	}
}