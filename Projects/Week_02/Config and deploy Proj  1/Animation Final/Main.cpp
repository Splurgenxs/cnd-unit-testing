#include <iostream>
#include "FactoryC.h"
#include "World.h"
#include "BatMan.h"
#include "iBadGuy.h"

#include "Catwoman.h"
#include "Joker.h"

#include "gtest/gtest.h"
#include "CND_DLL.h"

using namespace std;

BadGuy* vecDebug;
World* worldObj = new World();

//=====================================================================================

Catwoman cat1;// Test Case 1 (white Box) to check that the correct value is going through.
TEST(TestCase1, CheckingTheValue)
{
	EXPECT_EQ(150, cat1.getHealth());
	cat1.setHealth(50);
	EXPECT_EQ(100, cat1.getHealth());
}

//=====================================================================================

//Test Case 2 (white Box) Checking if the Random values are within range.
TEST(TestCase2, randomValueGeneratorCheck)
{
	cat1.GenBG2DPoint();
	EXPECT_GE(cat1.GetBG2DPoint().x, 1);
	EXPECT_LE(cat1.GetBG2DPoint().x, 10);
}

//Test Case 3 (white box) Checking if the Factory is creating object correctly.
TEST(TestCase3, FactoryGeneratedObjcts)
{
	FactoryC* BadGuysInWorld = new FactoryC();

	Catwoman cat2;
	EXPECT_EQ(cat2.getHealth(), BadGuysInWorld->AbCreateObject(3)->getHealth());
}

//Test Case 4 (white box) Testing the for loop for proper expected output values.
TEST(TestCase4, CheckForLoop)
{
	Catwoman cat3;
	for (int i = 0; i < 10; i++)
	{
		cat3.SetMoneyMAde(1000);
	}

	EXPECT_EQ(10000, cat3.MoneyMAde());
}

//Test Case 5 (white box) Testing the the Ids for the diffrent Joker objects are being set accordingly;
TEST(TestCase5, CheckforObjectIds)
{
	Joker joker1;
	EXPECT_EQ(100, joker1.getID());
	Joker joker2;
	EXPECT_EQ(101, joker2.getID());
	Joker joker3;
	EXPECT_EQ(102, joker3.getID());
	Joker joker4;
	EXPECT_EQ(103, joker4.getID());
}

//Test Case 6 (white box) Checking if the vector size increments as per the For loop its implemented in;
TEST(TestCase6, CheckforVectorSize)
{
	vector<BadGuy*>BadGuyVecList;
	FactoryC* BadGuysInWorld = new FactoryC();
	for (int index = 0; index < 20; index++)
	{
		srand(index);

		int randEnimies = rand() % 3 + 1;
		BadGuyVecList.push_back(BadGuysInWorld->AbCreateObject(randEnimies));
	}
	EXPECT_EQ(20, BadGuyVecList.size());
}

//Test Case 7 (white box) Checking for expected output for the multiplication function.
TEST(TestCase7, multiplyCoords)
{
	Vec2D tempVecObj;
	tempVecObj.x = 5; tempVecObj.y = 3;

	EXPECT_EQ(15, tempVecObj.vectorProduct(tempVecObj.x, tempVecObj.y));
}

//Test Case 8 (white box) Checking for expected output for the division function.
TEST(TestCase8, DivideCoords)
{
	Vec2D tempVecObj;
	tempVecObj.x = 6; tempVecObj.y = 3;

	EXPECT_EQ(2, tempVecObj.vectorDivide(tempVecObj.x, tempVecObj.y));
}

//=====================================================================================
//Test Case 1 (Black Box) Checking if the Add function runs correctly as per expectations.
DLLforCNDDLL myDllObj;
TEST(BlckBox1, checkForAdd)
{
	EXPECT_DOUBLE_EQ(12, myDllObj.Add(5, 7));
}

//Test Case 2 (Black Box) Checking if the Subtract function runs correctly as per expectations.
TEST(BlckBox2, checkForSubtract)
{
	EXPECT_DOUBLE_EQ(2, myDllObj.Subtract(7, 5));
}


//====================================================================================

void main(int argc, char **argv)
{
	 //worldObj->createBatman();
	 //worldObj->createBAdGuys();
	 //worldObj->gameLoop();

	::testing::InitGoogleTest(&argc, argv);
	RUN_ALL_TESTS();
	system("pause");
}