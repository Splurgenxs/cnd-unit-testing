#ifdef DLLforCNDDLL_EXPORTS
#define DLLforCNDDLL_API __declspec(dllexport) 
#else
#define DLLforCNDDLL_API __declspec(dllimport) 
#endif

class DLLforCNDDLL
{
public:
	// Returns a + b
	static DLLforCNDDLL_API double Add(double a, double b);

	// Returns a - b
	static DLLforCNDDLL_API double Subtract(double a, double b);

};