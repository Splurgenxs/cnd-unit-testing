#include "Catwoman.h"
#include <stdio.h>      
#include <stdlib.h>   
#include <time.h>    

int Catwoman::PID = 300;

Catwoman::Catwoman()
{
	CHealth = 150.0;

	this->SetIDs = PID;
	PID++;

	this->died = 0;
	this->money = 0;
}
Catwoman::~Catwoman(){}

void Catwoman::generateIDS(int& JID)
{
	JID++;
	this->SetIDs = this->PID;
}

int Catwoman::getID()
{
	return SetIDs;
}

Vec2D Catwoman::GetBG2DPoint()
{
	return BGpoint;
}

Vec2D Catwoman::GenBG2DPoint()
{

	srand(time(NULL));

	/*LO + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (HI - LO)));*/

	BGpoint.x = rand() % 10 + 1;
	BGpoint.y = rand() % 10 + 1;//10k by 10km matrix
	return BGpoint;
}

void Catwoman::setHealth(int damage)
{
	this->CHealth = CHealth - damage;
}

int Catwoman::getHealth()
{
	return	CHealth;
}

int Catwoman::MoneyMAde()
{
	return money;
}

int Catwoman::DeathCount()
{
	return died;
}


void Catwoman::takesPunch(int& damage, int& locChange)
{
	srand(time(NULL));
	damage = 10;
	locChange = rand() % 2 + 1;
}

void Catwoman::CauseMischeif(int& kChance, int& moneyLooted)
{
	srand(time(NULL));

	kChance = 0;
	int sChance = rand() % 4 + 1;
	if (sChance == 1)
	{
		int moneyLooted = rand() % 150000 + 5000;
	}
	else
	{
		moneyLooted = 0;
	}
}

void Catwoman::SetMoneyMAde(int value)
{
	money += value;
}

void Catwoman::SetDeathCount()
{
	 died++;
}
