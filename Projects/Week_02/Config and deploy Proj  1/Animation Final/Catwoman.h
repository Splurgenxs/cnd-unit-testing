#ifndef _catwoman_
#define _catwoman_

#include "iBadGuy.h"

class Catwoman : public BadGuy
{
public:
	Catwoman();
	~Catwoman();


	void generateIDS(int& ID);
	int getID();

	Vec2D GetBG2DPoint();
	Vec2D GenBG2DPoint();

	void takesPunch(int& damage, int& locChange);

	void setHealth(int damage);
	
	int getHealth();
	
	int MoneyMAde();
	int DeathCount();
	void CauseMischeif(int& kChance, int& moneyLooted);


	int money;
	int died;

	void SetMoneyMAde(int money);
	void  SetDeathCount();


private:

	static int PID;
	int SetIDs;

	int CHealth;

	Vec2D BGpoint;


	

};

#endif