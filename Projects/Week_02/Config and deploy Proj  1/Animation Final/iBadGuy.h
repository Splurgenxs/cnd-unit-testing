#ifndef _IbadGuy_
#define _IbadGuy_

#include "Vec2D.h"

class BadGuy
{
public:

	virtual ~BadGuy(){};


	virtual int getID() = 0;

	virtual Vec2D GetBG2DPoint() = 0;
	virtual Vec2D GenBG2DPoint() = 0;

	

	virtual void setHealth(int damage) = 0;
	virtual int getHealth() = 0;

	virtual void takesPunch(int& damage, int& locChange) = 0;
	virtual void CauseMischeif(int& kChance, int& sChance) = 0;

	virtual  int MoneyMAde() = 0;
	virtual  int  DeathCount() = 0;

	virtual  void SetMoneyMAde(int money) = 0;
	virtual  void  SetDeathCount() = 0;


	private:





};









#endif