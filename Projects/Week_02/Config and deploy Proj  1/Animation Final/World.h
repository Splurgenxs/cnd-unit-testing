#ifndef _World_
#define _World_

#include <vector>
#include "BatMan.h"
#include "FactoryC.h"
#include "Vec2D.h"

class World
{
public:
	World();
	~World();

	void createBatman();
	void createBAdGuys();
	
	BadGuy* findClosesEnemy();

	void gameLoop();

	vector<BadGuy*>BadGuyVecList;
    
	BatMan* BatfrWorld;

private:




};

#endif