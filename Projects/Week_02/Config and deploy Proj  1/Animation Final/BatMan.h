#ifndef _BTman_
#define _BTman_

#include "Vec2D.h"
#include "iBadGuy.h"
#include <vector>

using namespace std;

class BatMan
{
public:
	BatMan();
	~BatMan();

	bool spotBadguy(BadGuy* BGuy);
	void defendGotham();

	void setBatPosition(Vec2D BTPos);
	Vec2D GetBatPosition();

	Vec2D GenereateRandPos();

	bool checkForEnemy();
	void print();
private: 
	

	vector<BadGuy*>BGuyContainer;
	vector<BadGuy*> BGuyLef;
	Vec2D BTPos;



};

#endif