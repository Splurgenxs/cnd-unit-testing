#ifndef _penguin_
#define _penguin_



#include "iBadGuy.h"


class Penguin : public BadGuy
{
public:
	Penguin();
	~Penguin();


		void generateIDS(int& ID);
		int getID();

		Vec2D GetBG2DPoint();
		Vec2D GenBG2DPoint();

		void takesPunch(int& damage, int& locChange);
		
		void setHealth(int damage);
		int getHealth();

		void CauseMischeif(int& kChance, int& moneyLooted);
		int MoneyMAde();
		int DeathCount();
		int money;
		int died;

		void SetMoneyMAde(int money);
		void  SetDeathCount();

	private:
		
		static int PID;
		int SetIDs;


		int PHealth;
		Vec2D BGpoint;

	
};

#endif