#ifndef _FactoryC_
#define _FactoryC_

#include "AbBadGuyFactory.h"

class FactoryC : public BadGuyFactory
{
public:
	FactoryC();
	~FactoryC();

	BadGuy* AbCreateObject(int call);

private:

};

#endif