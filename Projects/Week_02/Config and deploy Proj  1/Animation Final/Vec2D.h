#ifndef _Vec2d_
#define _Vec2d_


class Vec2D
{
public:


	Vec2D(int x, int y);
	Vec2D();
	~Vec2D();

	int vectorProduct(int a, int b);
	int vectorDivide(int a, int b);

	int x, y;

private:


};

#endif