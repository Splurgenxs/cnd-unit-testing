#include <iostream>

#include "FactoryC.h"
#include "Catwoman.h"
#include "Joker.h"
#include "PenGuin.h"

using namespace std;

Vec2D f1;

FactoryC::FactoryC()
{
}
FactoryC::~FactoryC()
{
}

BadGuy* FactoryC::AbCreateObject(int call )
{
	if (call == 1)
	{
		
		BadGuy* JokerA = new Joker();
		 JokerA->getID();
		 JokerA->getHealth();
		 Vec2D Jloc = JokerA->GenBG2DPoint();
//================================================

		 //cout << "Joker" << " " << Jloc.x << "." << Jloc.y << endl;
		return JokerA;
		
	}
	else if (call == 2)
	{
		BadGuy* PenguinA = new Penguin();
		PenguinA->getID();
		PenguinA->getHealth();
		Vec2D Ploc = PenguinA->GenBG2DPoint();

		//cout << "Penguin" << " " << Ploc.x << "." << Ploc.y << endl;
		return PenguinA;
		
	}
	else if (call == 3)
	{
		BadGuy* CatWomanA = new Catwoman();
		
		int Cid = CatWomanA->getID();
		int Chelth = CatWomanA->getHealth();
		Vec2D Cloc = CatWomanA->GenBG2DPoint();
		//cout << "NewCatwoman" << " " << Cloc.x << "." << Cloc.y << endl;

		return CatWomanA;
		
	}


	
}