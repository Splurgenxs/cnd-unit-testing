#include "CGameObject.h"

// Initial value of the first CGameObject ID
// (i.e. the unique IDs of the objects will start at this number)
// Note that using an unsigned int, and assuming that it's 32 bits
//	means that it may potentially wrap around at some point. 
// (but you'd have to create 4 billion objects, first)
static const unsigned int FIRSTGAMEOBJECTID = 1000;
unsigned int CGameObject::m_nextID = FIRSTGAMEOBJECTID;


// Called by the constructors
void CGameObject::m_Init(void)
{
	// Assign ID here
	this->m_ID = this->m_nextID;   
	this->m_nextID++;	// Increment

	this->scale = 1.0f;

	this->name = "no name";
	this->plyFileName = "";

	this->Wireframe = false;


	return;
}

CGameObject::CGameObject()
{
	this->m_Init();

	this->m_pMediator = 0;

	//this->gmObj_ClothPhyObj = NULL;
	//gmObj_ClothPhyObj = new ClothPhysC();

}

void CGameObject::setMediator( IMediator* pMediator )
{
	// The mediator should only be set at construction, so if something
	//	tries to set it again, we prevent it
	if ( this->m_pMediator == 0 )
	{	// Hasn't been set, so assume creator is calling it
		this->m_pMediator = pMediator;
	}
	return;
}

CGameObject::~CGameObject()
{
	return;
}

void CGameObject::Update( float deltaTime )
{
	// Insert behaviour code in here
	int sexyBreakpoint = 0;

	return;
}

unsigned int CGameObject::getID(void)
{
	return this->m_ID;
}

void CGameObject::receiveMessage( CMessage theMessage, unsigned int senderID )
{
	CNameValuePair firstNVPair;

	if ( theMessage.findNVPairByName( "ChangeDirection", firstNVPair ) )
	{	// It's "Change direction", but which?
		if ( firstNVPair.sValue == "X" )
		{	// 2nd NV pair has velocity
			this->velocity.x = theMessage.vecNVPairs.at(1).fValue;
		}
		if ( firstNVPair.sValue == "Y" )
		{
			this->velocity.y = theMessage.vecNVPairs.at(1).fValue;
		}
		if ( firstNVPair.sValue == "Z" )
		{
			this->velocity.z = theMessage.vecNVPairs.at(1).fValue;
		}
	}
	else if ( theMessage.findNVPairByName( "StopMoving", firstNVPair ) )
	{
		this->velocity = CVector3f(0.0f, 0.0f, 0.0f);
	}

	return;
}


//
////=============For Cloth demo!!!!!Quick fix!! needs to be in its own class structure inhereting from GameObject============
//
//void CGameObject::getClothPhysics_Update(float dt)
//{
//	gmObj_ClothPhyObj->Update(dt);
//}
//
//void CGameObject::getCLothSImulate_clothDemo(std::vector<CVertex_fXYZ_fRGB>& vertices)
//{
//	gmObj_ClothPhyObj->ClothDemo(vertices);
//}
//
//void CGameObject::getClothSimulate_updatecloth()
//{
//	gmObj_ClothPhyObj->UpdateClothGraphics();
//}
//
//void CGameObject::getSetSprings(unsigned int idxA, unsigned int idxB, unsigned int idxC, unsigned int idxD)
//{
//	gmObj_ClothPhyObj->setSprings(idxA, idxB, idxC, idxD);
//}
////=============================================================================================================