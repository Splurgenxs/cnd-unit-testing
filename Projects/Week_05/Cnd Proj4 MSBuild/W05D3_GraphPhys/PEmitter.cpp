#include "PEmitter.h"

template <class T>
T getRand(T lowRange, T highRange)
{
	if (lowRange > highRange)
	{
		T temp = lowRange;
		lowRange = highRange;
		highRange = temp;
	}
	T delta = highRange - lowRange;
	delta = (static_cast<T>(rand()) / static_cast<T>(RAND_MAX)) * delta;
	return delta + lowRange;
}


ParticleEmitter::ParticleEmitter()
{
	
}

ParticleEmitter::~ParticleEmitter()
{
	return;
}

void ParticleEmitter::SetAcceleration(CVector3f newAcceleration)
{
	this->m_acceleration = newAcceleration;
	return;
}


void ParticleEmitter::SetLocation(CVector3f newLocation)
{
	// Emitter's location (not the particles)
	this->m_location = newLocation;
	return;
}

void ParticleEmitter::SetRespawn(bool bNewRespawn)
{
	this->m_bRespawn = bNewRespawn;
	return;
}

void ParticleEmitter::GenerateParticles(int numParticles, CVector3f initVelocity, float maxDistanceFromSouce, float startingAge, bool bRespawn)
{
	this->m_startingAge = startingAge;
	this->m_bRespawn = bRespawn;
	this->m_maxDistanceFromSouce = maxDistanceFromSouce;
	this->m_initVelocity = initVelocity;

	this->SphereParticles.clear();
	this->SphereParticles.reserve(numParticles);


	for (int count = 0; count != numParticles; count++)
	{
		FSphere tempParticle;
		m_CreateParticle(tempParticle);
		tempParticle.age = getRand(this->m_startingAge / 2.0f,
			this->m_startingAge);

		this->SphereParticles.push_back(tempParticle);
	}

	return;
}


void ParticleEmitter::GetSphParticles(std::vector <FSphere*> &vecLiveSParticles)
{
	vecLiveSParticles.clear();
	for (int index = 0; index != SphereParticles.size(); index++)
	{
		if (SphereParticles[index].age >= 0.0f)
		{
			vecLiveSParticles.push_back(&SphereParticles[index]);
		}
		else
		{

		
		}
	}
	return;
}

void ParticleEmitter::m_CreateParticle(FSphere &tempParticle)
{

	FSphere tempS;
	CVector3f Sobj;

	Sobj.x  = tempS.getCenter().x;
	Sobj.y = tempS.getCenter().y;
	Sobj.z =  tempS.getCenter().z;

	tempParticle.age = getRand(this->m_startingAge / 2.0f,
		this->m_startingAge);

	Sobj.x
		= this->m_location.x
		+ getRand<float>(-(this->m_maxDistanceFromSouce), this->m_maxDistanceFromSouce);

	Sobj.y
		= this->m_location.y
		+ getRand<float>(-(this->m_maxDistanceFromSouce), this->m_maxDistanceFromSouce);

	Sobj.z
		= this->m_location.z
		+ getRand<float>(-(this->m_maxDistanceFromSouce), this->m_maxDistanceFromSouce);


	tempParticle.velocity.x = getRand<float>(-(this->m_initVelocity.x), this->m_initVelocity.x);
	tempParticle.velocity.y = getRand<float>(-(this->m_initVelocity.y), this->m_initVelocity.y);
	tempParticle.velocity.z = getRand<float>(-(this->m_initVelocity.z), this->m_initVelocity.z);
	tempParticle.setCenter(Sobj);
	return;
}


void ParticleEmitter::Update(float deltaTime)
{

	for (int index = 0; index != this->SphereParticles.size(); index++)
	{

		this->SphereParticles[index].age -= deltaTime;

		if (this->SphereParticles[index].age <= 0)
		{	
			if (this->m_bRespawn)
			{	
				this->m_CreateParticle(this->SphereParticles[index]);
			}
		}

        
		if (this->SphereParticles[index].age >= 0)
		{

			CVector3f center = this->SphereParticles[index].getCenter();
		
			this->SphereParticles[index].velocity.x += (this->m_acceleration.x * deltaTime);
			this->SphereParticles[index].velocity.y += (this->m_acceleration.y * deltaTime);
			this->SphereParticles[index].velocity.z += (this->m_acceleration.z * deltaTime);

			center.x += (this->SphereParticles[index].velocity.x * deltaTime);
			center.y += (this->SphereParticles[index].velocity.y * deltaTime);
			center.z += (this->SphereParticles[index].velocity.z * deltaTime);

			SphereParticles[index].setCenter(center);
		}
	}

	return;
}

