#ifndef _PEmitter_
#define _PEmitter_

#include "CVector3f.h"
#include <vector>
#include "FSphere.h"


class ParticleEmitter
{
public:
	ParticleEmitter();
	~ParticleEmitter();
	void SetLocation(CVector3f newLocation);
	void SetAcceleration(CVector3f newAcceleration);
	void GenerateParticles(int numParticles, CVector3f initVelocity,float maxDistanceFromSouce,float startingAge,bool bRespawn);
	void SetRespawn(bool bNewRespawn);		
	void Update(float deltaTime);


	// "Shouldn't" make this public, but for time, we'll do it
	class CParticle
	{
	public:
		CParticle() {};	// construtor defined here
		~CParticle() {};	// 
		CVector3f position;		// c'tor sets these to zeros
		CVector3f velocity;		// c'tor sets these to zeros
		float age;
	};
	// Returns any "alive" particles
	//void GetParticles(std::vector< CParticle > &vecLiveParticles);

	void GetSphParticles(std::vector< FSphere* > &vecLiveSParticles);
	void m_CreateParticle(FSphere &tempParticle);
	
	std::vector< CParticle > m_myParticles;
	std::vector< FSphere > SphereParticles;


	bool Alive();
private:

	CVector3f m_location;
	CVector3f m_acceleration;
	// 
	CVector3f m_initVelocity;
	float m_maxDistanceFromSouce;
	float m_startingAge;
	bool m_bRespawn;

	// Called internally
	void m_CreateParticle(CParticle &tempParticle);
};

#endif
