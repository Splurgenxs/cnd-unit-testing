//#pragma once
//#ifndef _ClothPhysC_
//#define _ClothPhysC_
//
//#include "CPlyInfo.h"
//#include <vector>
//#include <sstream>
//#include <iostream>
//#include <map>
//#include <glm/glm.hpp>
//#include "VertexTypes.h"
//
//class ClothPhysC
//{
//public:
//	ClothPhysC();
//	~ClothPhysC();
//
//	struct sNode;
//	struct sSpring
//	{
//		sSpring();
//		sSpring(sNode* nodeA, sNode* nodeB, float length);
//		sNode* NodeA;
//		sNode* NodeB;
//		float RestLength;
//		float Length;
//	};
//
//	struct sNode
//	{
//		
//		sNode(unsigned int idxVert, float x, float y, float z);
//		glm::vec3 Position;
//		glm::vec3 PreviousPosition;
//		glm::vec3 Velocity;
//		glm::vec3 Acceleration;
//		glm::vec3 SpringForce;
//		bool FixedPosition;
//		unsigned int IdxVert;
//		std::map<unsigned int, sSpring*> Springs;
//	};
//	
//
//	virtual void Update(float dt);
//	void GetPosition(unsigned int idx, float* floatXYZ);
//	void SetNodeFixed(unsigned int idx, bool value);
//	void SetVertices(const std::vector<CVertex_fXYZ_fRGB>& vertices, float radius);
//	void SetSpringsFromTriangulatedIndices(const std::vector<unsigned int>& indices);
//	void AddSpring(unsigned int idxA, unsigned int idxB);
//
//
//	//=============cLOTH sIMULATE==========
//	void setSprings(unsigned int idxA, unsigned int idxB, unsigned int idxC, unsigned int idxD);
//	void ClothDemo(std::vector<CVertex_fXYZ_fRGB>& vertices);
//
//	CVertex_fXYZ_fRGB* BeginMapping();
//	void EndMapping();
//	void UpdateClothGraphics();
//
//	//=====================================
//
//
//
//
//private:
//
//	std::vector<sNode*> mNodes;
//	std::vector<sSpring*> mSprings;
//
//	float mTime;
//	float mRadius;
//	glm::vec3 mGravity;
//	float mDamping; // spring damping
//	float mK;// spring constant
//	float mMass;// mass per vertex
//	glm::vec3 mWind;// wind vector
//
//	void CalculateSpringForces();
//	void SetSpringLengths();
//
//
//	GLuint numVerts;
//};
//
//#endif