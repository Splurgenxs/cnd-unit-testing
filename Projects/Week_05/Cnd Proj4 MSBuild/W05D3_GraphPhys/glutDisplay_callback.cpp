#include "global.h"

#include <glm/gtc/matrix_transform.hpp> // glm::translate, glm::rotate, glm::scale, glm::perspective
#include <glm/gtc/type_ptr.hpp> // glm::value_ptr


void glutDisplay_callback(void)
{
	//++FrameCount;
	::g_FrameCount++;

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	clock_t Now = clock();
	if (::g_LastTime == 0)	// if (LastTime == 0)
	{
		::g_LastTime = Now;
	}	// LastTime = Now;

	//========================= Define where the view is set from , ie the Camera ====================
	glm::mat4 matView(1.0f);
	matView = glm::lookAt(glm::vec3(g_pCamera->eye.x, g_pCamera->eye.y, g_pCamera->eye.z),				// Camera (aka "Eye")
		glm::vec3(g_pCamera->target.x, g_pCamera->target.y, g_pCamera->target.z),	// At (aka "target")
		glm::vec3(g_pCamera->up.x, g_pCamera->up.y, g_pCamera->up.z));				// Up
	//=================================================================================================

	// Get the objects we are to render from the factory
	std::vector< CGameObject* > vec_pRenderedObjects;
	g_pFactoryMediator->getRenderedObjects(vec_pRenderedObjects);
	
	// min max
	int min;
		int max;

	if (d_keyretrun == 0)
	{
		min = 0;
		max = 1;
	}
	if (d_keyretrun == 1)
	{
		min = 1;
		max = 2;
	}
	if (d_keyretrun == 2)
	{
		min = 2;
		max = 3;
	}
	if (d_keyretrun == 3)
	{
		min = 3;
		max = 3;
		
	}

	for (int index = min; index <= max /* index != static_cast<int>(vec_pRenderedObjects.size())*/; index++)
		{

			SetShaderUniformVariables();
			//ModelMatrix = IDENTITY_MATRIX;		/// aka "World" 
			glm::mat4 matWorld = glm::mat4(1.0f);		// aka "World" matrix

			// Set up separate matrices for each type of tranformation
			// Then combine them (by multiplying) into the world matrix.
			// The order you do this impact what you see. 
			// (The last thing you do is the FIRST thing that's applied)
			// (TIP: Put scaling last)
			// (TIP: pre-Rotation, Translation, post-Rotation )
			matWorld = glm::translate(matWorld, glm::vec3(vec_pRenderedObjects[index]->position.x,
				vec_pRenderedObjects[index]->position.y,
				vec_pRenderedObjects[index]->position.z));

			matWorld = glm::rotate(matWorld, vec_pRenderedObjects[index]->rotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
			matWorld = glm::rotate(matWorld, vec_pRenderedObjects[index]->rotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
			matWorld = glm::rotate(matWorld, vec_pRenderedObjects[index]->rotation.z, glm::vec3(0.0f, 0.0f, 1.0f));


			matWorld = glm::scale(matWorld, glm::vec3(vec_pRenderedObjects[index]->scale,
				vec_pRenderedObjects[index]->scale,
				vec_pRenderedObjects[index]->scale));

			// *** START of DRAW THE OBJECT FROM THE BUFFER ****

			if (vec_pRenderedObjects[index]->Wireframe == true)//check for property every loop
			{
				glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
			}
			else
			{
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			}

			//glUseProgram(g_ShaderIds[0]);
			glUseProgram(g_ShaderProgram_ID);
			ExitOnGLError("ERROR: Could not use the shader program");

			glUniformMatrix4fv(g_ModelMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(matWorld));
			glUniformMatrix4fv(g_ViewMatrixUniformLocation, 1, GL_FALSE, glm::value_ptr(matView));

			glUniform4f(g_ObjectColourUniformLocation, vec_pRenderedObjects[index]->colour.x, vec_pRenderedObjects[index]->colour.y, vec_pRenderedObjects[index]->colour.z, 1.0f);	// Colour has 4 components (vec4)

			// Light position
			glUniform4f(g_slot_LightPosition, 1.0f, 1.0f, 0.0f, 1.0f);
			// White light
			glUniform4f(g_slot_LightColour, 1.0f, 1.0f, 1.0f, 1.0f);

			float atten = 1.0f / 25.0f;	//
			glUniform1f(g_slot_attenuation, atten);


			ExitOnGLError("ERROR: Could not set the shader uniforms");

			//glBindVertexArray(BufferIds[0]);	
			// Based on the object "ply file" name, point to the appropriate buffer

			int numberOfIndicesToRender = 0;

			CPlyInfo tempPlyInfo;
			std::string plyFileNameDEBUG = vec_pRenderedObjects[index]->plyFileName;
			if (!::g_pModelLoader->GetRenderingInfoByModelFileName(vec_pRenderedObjects[index]->plyFileName,
				tempPlyInfo))
			{	// Model isn't present, which is a Very Bad Thing
				continue;	// in a for loop, this will go to the next count, skipping everyint
			}
			// At this point, we have found a valid model (that was loaded)
			glBindVertexArray(tempPlyInfo.vertexPositionBufferID);					//  g_VertexPositionBufferID[1] );				// "Connects" to a vertex buffer
			glBindBuffer(GL_ARRAY_BUFFER, tempPlyInfo.vertexColourBufferID);	//  g_VertexColourBufferID[1] );		// Also buffer (with colours)
			glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, tempPlyInfo.indexBufferID);	// g_IndexBufferID[1] );	// And Index buffer	
			numberOfIndicesToRender = tempPlyInfo.numberOfElements * 3;			// g_numberOfTriangles[1] * 3; 


			glEnableVertexAttribArray(0);			// Position
			glEnableVertexAttribArray(1);			// Colour
			ExitOnGLError("ERROR: Could not enable vertex attributes");

			glVertexAttribPointer(0,		// index or "slot" in the shader
				4,		// Number of variables: vec4 would be 4 32-bit variables
				GL_FLOAT,	// Type: vec4 is float
				GL_FALSE, // "normalize" the values (or not)
				sizeof(CVertex_fXYZ_fRGB),	// Number of bytes per vertex (the "stride")
				(GLvoid*)0);		// Offset from vertex (position is first, so offset = 0
			ExitOnGLError("ERROR: Could not set VAO attributes");

			int offsetToColourInBytes = sizeof(((CVertex_fXYZ_fRGB*)0)->Color);

			glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(CVertex_fXYZ_fRGB), (GLvoid*)offsetToColourInBytes);

			ExitOnGLError("ERROR: Could not set VAO attributes");

			//int numberOfIndicesToRender = g_numberOfTriangles[1] * 3; 
			//To finally reder objects
			glDrawElements(GL_TRIANGLES, numberOfIndicesToRender, GL_UNSIGNED_INT, (GLvoid*)0);
			ExitOnGLError("ERROR: Could not draw the cube");
			// *** END of DRAW THE OBJECT FROM THE BUFFER ****

			glDisableVertexAttribArray(0);
			glDisableVertexAttribArray(1);



			// Detatch from the vertex buffer (0 is reserved in OpenGL, so setting to "0" means "set to nothing", sort of)
			glBindVertexArray(0);
			// Detatch from the current shader program
			glUseProgram(0);


		}

		//-------------------------------------CND PROJ 3 Done---------------------------------------------------
		glEnable(GL_TEXTURE_2D);
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glOrtho(0.0, 1200, 0.0, 800, 5, -5);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();


		eLanguage cLang;

		if (_currentLanguage == LANGUAGE_NONE && KeyCalbackReturn != 1)
		{

			glColor3f(1.0f, 1.0f, 1.0f);
			_gl_font.Begin();
			_gl_font.DrawString("Press 'O' to Display Contents", 1.0f, 0.0f, static_cast<float>(g_screenHeight));

			if (pauseState == true)
			{
				_gl_font.Begin();
				_gl_font.DrawString("Game is Paused , press 'P' agian to play", 1.0f, 0.0f, static_cast<float>(g_screenHeight)-(80.0f * 7));
			}
		}
		else if (KeyCalbackReturn == 1 && _currentLanguage == LANGUAGE_NONE)
		{
			GLfloat yOffset = 80.0f;

			glColor3f(1.0f, 1.0f, 1.0f);
			_gl_font.Begin();
			_gl_font.DrawString("Press - key 1 - for - ENGLISH", 1.0f, 0.0f, static_cast<float>(g_screenHeight));
			_gl_font.DrawString("Press - key 2 - for - FRENCH", 1.0f, 0.0f, static_cast<float>(g_screenHeight)-(yOffset));
			_gl_font.DrawString("Press - key 3 - for - SPANISH", 1.0f, 0.0f, static_cast<float>(g_screenHeight)-(yOffset * 2));
			_gl_font.DrawString("Press - key 4 - for - HINDI", 1.0f, 0.0f, static_cast<float>(g_screenHeight)-(yOffset * 3));

			if (pauseState == true)
			{
				_gl_font.Begin();
				_gl_font.DrawString("Game is Paused , press 'P' agian to play", 1.0f, 0.0f, static_cast<float>(g_screenHeight)-(yOffset * 7));
			}
		}
		else
		{
			GLfloat yOffset = 80.0f;

			glColor3f(1.0f, 1.0f, 1.0f);
			_gl_font.Begin();
			_gl_font.DrawString(_mapLanguage[_currentLanguage][(GLuint)STRING_1], 1.0f, 0.0f, static_cast<float>(g_screenHeight));
			_gl_font.DrawString(_mapLanguage[_currentLanguage][(GLuint)STRING_2], 1.0f, 0.0f, static_cast<float>(g_screenHeight)-(yOffset * 2));
			_gl_font.DrawString(_mapLanguage[_currentLanguage][(GLuint)STRING_3], 1.0f, 0.0f, static_cast<float>(g_screenHeight)-(yOffset * 3));
			_gl_font.DrawString(_mapLanguage[_currentLanguage][(GLuint)STRING_4], 1.0f, 0.0f, static_cast<float>(g_screenHeight)-(yOffset * 4));
			_gl_font.DrawString(_mapLanguage[_currentLanguage][(GLuint)STRING_5], 1.0f, 0.0f, static_cast<float>(g_screenHeight)-(yOffset * 5));

			if (pauseState == true)
			{
				_gl_font.DrawString("Game is Paused , press 'P' agian to play", 1.0f, 0.0f, static_cast<float>(g_screenHeight)-(yOffset * 7));
			}
		}

		//------------------------------------------CND PROJ 3 Done----------------------------------------
		glutSwapBuffers();
		//---------------------------------------------------------	--------------------------------------
		/*_gl_font.Destroy();*/

		// Now that everything is drawn, show the back buffer
		// (i.e. switch the 'back' framebuffer with the 'front')


		return;
}


