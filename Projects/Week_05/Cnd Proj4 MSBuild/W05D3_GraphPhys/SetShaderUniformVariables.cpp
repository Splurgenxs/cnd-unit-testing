#include "global.h"

#include <vector>
#include <fstream>



bool SetShaderUniformVariables(void)
{
	//g_ModelMatrixUniformLocation = glGetUniformLocation(g_ShaderIds[0], "ModelMatrix");
	g_ModelMatrixUniformLocation = glGetUniformLocation( g_ShaderProgram_ID, "ModelMatrix");

	//g_ViewMatrixUniformLocation = glGetUniformLocation(g_ShaderIds[0], "ViewMatrix");
	g_ViewMatrixUniformLocation = glGetUniformLocation(g_ShaderProgram_ID, "ViewMatrix");


	//g_ProjectionMatrixUniformLocation = glGetUniformLocation(g_ShaderIds[0], "ProjectionMatrix");
	g_ProjectionMatrixUniformLocation = glGetUniformLocation(g_ShaderProgram_ID, "ProjectionMatrix");

	// Added on September 29. 
	// Ask OpenGL the location of our uniform variable.
	//g_ObjectColourUniformLocation = glGetUniformLocation( g_ShaderIds[0], "objectColour" );
	g_ObjectColourUniformLocation = glGetUniformLocation( g_ShaderProgram_ID, "objectColour" );

	//g_slot_LightPosition = glGetUniformLocation( g_ShaderIds[0], "LightPosition" );//uniform vec4 LightPosition;
	g_slot_LightPosition = glGetUniformLocation( g_ShaderProgram_ID, "LightPosition" );//uniform vec4 LightPosition;
	//g_slot_LightColour   = glGetUniformLocation( g_ShaderIds[0], "LightColour" );	 //uniform vec4 LightColour; 
	g_slot_LightColour   = glGetUniformLocation( g_ShaderProgram_ID, "LightColour" );	 //uniform vec4 LightColour; 
	//g_slot_attenuation   = glGetUniformLocation( g_ShaderIds[0], "attenuation" );  //uniform float attenuation;
	g_slot_attenuation   = glGetUniformLocation( g_ShaderProgram_ID, "attenuation" );  //uniform float attenuation;
	ExitOnGLError("ERROR: Could not get shader uniform locations");

	return true;
}

//Moved to Cmodelloadmanger...