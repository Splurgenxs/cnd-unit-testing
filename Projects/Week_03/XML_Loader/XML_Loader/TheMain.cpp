
#include "pugixml.hpp"

#include <iostream>
#include <string>

std::string _xmlFileName = "gameConfigSettings.xml";

int main()
{
	pugi::xml_document doc;

	pugi::xml_parse_result result = doc.load_file(_xmlFileName.c_str());

	if (result)
	{
		std::cout << "XML loaded succesfully!" << std::endl;
	}
	else
	{
		std::cout << "Error, Unable to load XML file!" << std::endl;
		
		return -1;
	}

	pugi::xml_node plyfiles = doc.child("ConfigSettings").child("PlyFiles");

	for (pugi::xml_node_iterator it = plyfiles.begin(); it != plyfiles.end(); it++)
	{
		std::cout << "-- Ply File --" << std::endl;
		if (it->child("Description"))
		{
			std::cout << "Description: " << it->child("Description").child_value() << std::endl;
		}
		//Loop attributes
		for (pugi::xml_attribute_iterator ait = it->attributes_begin(); ait != it->attributes_end(); ait++)
		{
			std::cout << " " << ait->name() << "=" << ait->value() << std::endl;
		}
	}

	int b = 0; 
	return 0;
}