#include "simulate.h"
#include "pugixml.hpp"
#include <iostream>
#include <conio.h>

#include <time.h>

simulate::simulate()
{
	
	collision = false;
}
simulate::~simulate()
{
}


void simulate::setValues(std::string filename)
{


	_xmlFileName = filename;
		pugi::xml_document doc;

		pugi::xml_parse_result result = doc.load_file(_xmlFileName.c_str());

		if (result)
		{
			std::cout << "XML loaded succesfully!" << std::endl;
		}
		else
		{
			std::cout << "Error, Unable to load XML file!" << std::endl;
			/*return -1;*/
		}

		//===================================Player================================

		pugi::xml_node plyfiles = doc.child("PeopleTypes").child("Players");

		for (pugi::xml_node_iterator it = plyfiles.begin(); it != plyfiles.end(); it++)
		{
			for (pugi::xml_attribute_iterator ait = it->attributes_begin(); ait != it->attributes_end(); ait++)
			{
				std::string name = ait->name();
				if (name == "name")
				{
					playerObj.setPlayername(ait->value());
				}
				VecClass tempobj;
				if (name == "posX")
				{
					std::string s;
					s = ait->value();
					tempobj.x = std::stoi(s);
				}
				if (name == "posY")
				{
					std::string s;
					s = ait->value();
					tempobj.y = std::stoi(s);
				}
				playerObj.setPLayerPos(tempobj);

				if (name == "health")
				{
					std::string s;
					s = ait->value();
					int health = std::stoi(s);

					playerObj.setPlayerHelath(health);
				}
				if (name == "power")
				{
					std::string s;
					s = ait->value();
					int power = std::stoi(s);

					playerObj.setPlayerpowr(power);
				}

				if (name == "sex")
				{
					std::string s;
					s = ait->value();
					char sex = s.c_str()[0];

					playerObj.setPlayerSex(sex);
				}

				if (name == "age")
				{
					std::string s;
					s = ait->value();
					int age = std::stoi(s);

					playerObj.setPLayerAge(age);
				}

				if (name == "color")
				{
					playerObj.setplayerColor(ait->value());
				}

				if (name == "speed")
				{
					std::string s;
					s = ait->value();
					int speed = std::stoi(s);

					playerObj.setplayerSpeed(speed);
				}

				if (name == "jumpstr")
				{
					std::string s;
					s = ait->value();
					int jumpstrength = std::stoi(s);

					playerObj.setplayerJumpStr(jumpstrength);
				}

				if (name == "agility")
				{
					std::string s;
					s = ait->value();
					int agility = std::stoi(s);

					playerObj.setplayerAgility(agility);
				}

			}
		}
		//==============================================================

		plyfiles = doc.child("PeopleTypes").child("Enemys");
		for (pugi::xml_node_iterator it = plyfiles.begin(); it != plyfiles.end(); it++)
		{
			for (pugi::xml_node_iterator it = plyfiles.begin(); it != plyfiles.end(); it++)
			{
				for (pugi::xml_attribute_iterator ait = it->attributes_begin(); ait != it->attributes_end(); ait++)
				{

					std::string name = ait->name();
					if (name == "name")
					{
						enemyObj.setEnemyType(ait->value());//-------->-

					}
					VecClass tempobj;
					if (name == "posX")
					{

						std::string s;
						s = ait->value();
						tempobj.x = std::stoi(s);
					}
					if (name == "posY")
					{
						std::string s;
						s = ait->value();
						tempobj.y = std::stoi(s);
					}
					enemyObj.setEemyPos(tempobj);//--------->

					if (name == "health")
					{
						std::string s;
						s = ait->value();
						int health = std::stoi(s);

						enemyObj.setEnemyHelath(health);
					}
					if (name == "power")
					{
						std::string s;
						s = ait->value();
						int power = std::stoi(s);

						enemyObj.setEnemyPower(power);
					}

					if (name == "sex")
					{
						std::string s;
						s = ait->value();
						char sex = s.c_str()[0];

						enemyObj.setenemySex(sex);
					}

					if (name == "age")
					{
						std::string s;
						s = ait->value();
						int age = std::stoi(s);

						enemyObj.setenemyAge(age);
					}

					if (name == "color")
					{
						enemyObj.setenemyColor(ait->value());
					}

					if (name == "speed")
					{
						std::string s;
						s = ait->value();
						int speed = std::stoi(s);

						enemyObj.setenemySpeed(speed);
					}

					if (name == "jumpstr")
					{
						std::string s;
						s = ait->value();
						int jumpstrength = std::stoi(s);

						enemyObj.setEnemyPower(jumpstrength);
					}

					if (name == "agility")
					{
						std::string s;
						s = ait->value();
						int agility = std::stoi(s);

						enemyObj.setenemyAgility(agility);
					}

				}
			}
		}
	}





void simulate::saveFunct(std::string filename)
{

	_xmlFileName = filename;
	pugi::xml_document doc;

	pugi::xml_parse_result result = doc.load_file(_xmlFileName.c_str());

	pugi::xml_node	plyfiles = doc.child("PeopleTypes").child("Enemys");
	for (pugi::xml_node_iterator it = plyfiles.begin(); it != plyfiles.end(); it++)
	{
			for (pugi::xml_attribute_iterator ait = it->attributes_begin(); ait != it->attributes_end(); ait++)
			{
				std::string name = ait->name();


				std::cout << "Games Saved" << std::endl;
				int enemyX = enemyObj.getEnemyPos().x;

				if (name == "posX")
				{
					ait->set_value(enemyX);//---->saving


					std::cout << "Enemypos" << enemyObj.getEnemyPos().x << "," << enemyObj.getEnemyPos().y << std::endl;
					break;
				}
			}
	}


	   plyfiles = doc.child("PeopleTypes").child("Players");
	for (pugi::xml_node_iterator it = plyfiles.begin(); it != plyfiles.end(); it++)
	{
			for (pugi::xml_attribute_iterator ait = it->attributes_begin(); ait != it->attributes_end(); ait++)
			{
				std::string name = ait->name();


				int playerX = playerObj.getPLayerPos().x;

				if (name == "posX")
				{
					ait->set_value(playerX);// --->saving

					std::cout << "Playrpos" << playerObj.getPLayerPos().x << "," << playerObj.getPLayerPos().y << std::endl;

					break;
				}
			}
	}

	doc.save_file("mySaveXml.xml");
}

void simulate::simulation(std::string filename)
{
	pugi::xml_document doc;

	while (collision == false)
	{

		VecClass playerPosObj = playerObj.getPLayerPos();
		playerPosObj.x -= 1;
		playerObj.setPLayerPos(playerPosObj);
		//ait->set_value(playerPosObj.x);//----------------------->
		std::cout << ".";

		_sleep(1000);
	
		VecClass enemyPosObj = enemyObj.getEnemyPos();
		enemyPosObj.x += 1;
		enemyObj.setEemyPos(enemyPosObj);
		//ait->set_value(enemyPosObj.x);//-----------------------
		std::cout << ".";

		std::cout << " press S/s to save or any other key to continue" << std::endl;
		char choice = _getch();

		if (choice == 's' || choice == 'S')
		{
			saveFunct(filename);
		}

		if(playerObj.getPLayerPos().x == enemyObj.getEnemyPos().x)
		{
			collision = true;
			std::cout << "!!!!!!!!!!!.................Fighting has begun..............!!!!!!!!!!!!!!" << std::endl;
		}
	}


}

