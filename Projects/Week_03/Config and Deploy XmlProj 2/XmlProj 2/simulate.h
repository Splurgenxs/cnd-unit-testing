#ifndef _simulate_
#define _simulate_


#include "Enemy.h"
#include "player.h"



class simulate
{
public:
	simulate();
	~simulate();



	void setValues(std::string filename);
	void simulation(std::string filename);

private:

	Enemy enemyObj;
	player playerObj;

	std::string _xmlFileName;
	
	bool collision;


	void saveFunct(std::string filename);




};


#endif

