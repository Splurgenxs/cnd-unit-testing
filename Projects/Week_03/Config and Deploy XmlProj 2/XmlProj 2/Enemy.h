#pragma once

#ifndef _Enemy_
#define _Enemy_

#include <string>
#include "VecClass.h"

class Enemy
{
public:
	Enemy();
	~Enemy();

	void setEnemyType(std::string enemyType);
	std::string getEnemyType();

	void setEnemyPower(int enemyPower);
	int getEnemyPower();

	void setEnemyHelath(int enemyHealth);
	int getenemyHealth();

	void setEemyPos(VecClass enemyPos);
	VecClass getEnemyPos();
	
	void setenemyColor(std::string enemyColor);
	std::string getenemyColor();

	int getenemyAge();
	void setenemyAge(int enemyAge);

	char getenemySex();
	void setenemySex(char enemySex);

	int getenemySpeed();
	void setenemySpeed(int enemySpeed);

	int getenemyJumpStr();
	void setenemyJumpStr(int enemyJumpStr);

	int  getenemyAgility();
	void  setenemyAgility(int  enemyAgility);

private : 

	std::string enemyType;
	int enemyPower;
	int enemyHealth;
	VecClass enemyPos;

	std::string enemyColor;
	int enemyAge;
	char enemySex;

	int enemySpeed;
	int enemyJumpStr;
	int  enemyAgility;

};

#endif