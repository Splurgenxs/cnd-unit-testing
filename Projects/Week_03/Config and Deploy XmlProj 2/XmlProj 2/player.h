#pragma once

#ifndef _player_
#define _player_

#include "VecClass.h"

#include <string>
class player
{
public:
	player();
	~player();

	void setPlayername(std::string playername);
	std::string getPlayername();
	
	void setPLayerPos(VecClass playerPos);
	VecClass getPLayerPos();
	
	void setPlayerHelath(int playHealth);
	int getPlayerHelath();

	void setPlayerpowr(int playerPower);
	int getPlayerpowr();

	void setPlayerSex(char playersex);
	char getPlayerSex();

	void setPLayerAge(int playerAge);
	int getPLayerAge();

	void setplayerColor(std::string playerColor);
	std::string getplayerColor();

	void setplayerSpeed(int playerSpeed);
	int getplayerSpeed();

	void setplayerJumpStr(int playerJumpStr);
	int getplayerJumpStr();
	
	void setplayerAgility(int  playerAgility);
	int getplayerAgility();

private:
	
	std::string playername;
	VecClass playerPos;
	int playHealth;
	int playerPower;

	char playersex;
	int playerAge;
	std::string playerColor;

	int playerSpeed;
	int playerJumpStr;
	int  playerAgility;

};

#endif