#include "Enemy.h"


Enemy::Enemy()
{
}

Enemy::~Enemy()
{
}


void Enemy::setEnemyType(std::string enemyType)
{
	this->enemyType = enemyType;
}
std::string Enemy::getEnemyType()
{
	return enemyType;
}

void Enemy::setEnemyPower(int enemyPower)
{
	this->enemyPower = enemyPower;
}
int Enemy::getEnemyPower()
{
	return enemyPower;
}

void Enemy::setEnemyHelath(int enemyHealth)
{
	this->enemyHealth = enemyHealth;
}
int Enemy::getenemyHealth()
{
	return enemyHealth;
}

void Enemy::setEemyPos(VecClass enemyPos)
{
	this->enemyPos = enemyPos;
}
VecClass Enemy::getEnemyPos()
{
	return enemyPos;
}

void Enemy::setenemyColor(std::string enemyColor)
{
	this->enemyColor = enemyColor;
}
std::string Enemy::getenemyColor()
{
	return enemyColor;
}

int Enemy::getenemyAge()
{
	return enemyAge;
}
void Enemy::setenemyAge(int enemyAge)
{
	this->enemyAge = enemyAge;
}

char Enemy::getenemySex()
{
	return enemySex;
}
void Enemy::setenemySex(char enemySex)
{
	this->enemySex = enemySex;
}

int Enemy::getenemySpeed()
{
	return enemySpeed;
}
void Enemy::setenemySpeed(int enemySpeed)
{
	this->enemySpeed = enemySpeed;
}

int Enemy::getenemyJumpStr()
{
	return enemyJumpStr;
}
void Enemy::setenemyJumpStr(int enemyJumpStr)
{
	this->enemyJumpStr = enemyJumpStr;
}

int  Enemy::getenemyAgility()
{
	return enemyAgility;
}
void  Enemy::setenemyAgility(int  enemyAgility)
{
	this->enemyAgility = enemyAgility;
}