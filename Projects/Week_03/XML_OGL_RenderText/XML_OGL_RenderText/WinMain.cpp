

#include <Windows.h>
#include <stdio.h>

#include <GLFW\glfw3.h>

#include "glfont2.h"

int _windowWidth = 800;
int _windowHeight = 600;

glfont::GLFont gl_font;

static void error_callback(int error, const char* description)
{
	fputs(description, stderr);
}

static void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
	{
		glfwSetWindowShouldClose(window, GL_TRUE);
	}
}

void renderText()
{
	float top_color[3] = {1.0f, 1.0f, 1.0f};
	float botton_color[3] = {0.0f, 0.0f, 1.0f};

	//Clear back buffer
	glClear(GL_COLOR_BUFFER_BIT);
	//Initialize opengl matrix. 
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, _windowWidth, 0.0, _windowHeight, 5, -5);
	//Initialize modelview matrix
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//Draw some strings
	glColor3f(1.0f, 1.0f, 1.0f);
	gl_font.Begin();
	gl_font.DrawString("Hello Configuration and deployment", 2.0f, 0.0f, static_cast<float>(_windowHeight));
}

int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{

	GLFWwindow* window;
	glfwSetErrorCallback(error_callback);
	if (!glfwInit())
		exit(EXIT_FAILURE);
	window = glfwCreateWindow(_windowWidth, _windowHeight, "XML render text example", NULL, NULL);
	if (!window)
	{
		glfwTerminate();
		exit(EXIT_FAILURE);
	}
	glfwMakeContextCurrent(window);
	glfwSwapInterval(1);
	glfwSetKeyCallback(window, key_callback);


	glClearColor(0.0, 0.0, 0.0, 1.0);
	glEnable(GL_TEXTURE_2D);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	//TODO: LOAD FONT FILE HERE.
	if (!gl_font.Create("arial22Font.glf", 1))
	{
		return FALSE;
	}

	while (!glfwWindowShouldClose(window))
	{
		float ratio;
		int width, height;
		glfwGetFramebufferSize(window, &width, &height);
		ratio = width / (float)height;
		
		//TODO: CALL RENDER TEXT
		renderText();
		glfwSwapBuffers(window);
		glfwPollEvents();
	}

	glfwDestroyWindow(window);
	glfwTerminate();
	exit(EXIT_SUCCESS);

	return 0;
}